<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    if (!isset($_SESSION['id_login'])) {
      redirect('login');
    }
    $this->load->model('Model_transaction', 'transaction');
    $this->load->model('Model_master', 'master');
  }

  public function index()
  {
    $data['javascript'] = 'transaction.js';
    $data['item'] = $this->master->get_available_item();
    $data['uom'] = $this->master->get_uom();
    $data['payment'] = $this->master->get_payment();
    $this->template->load('template', 'transaction', $data);
  }

  public function get_cart_item()
  {
    $list = $this->transaction->get_data_cart_item();

    $data = array();
    $no = $_POST['start'];

    foreach ($list as $ls) {
      $no++;
      $row = array();

      if($ls['flag'] == ''){
        $flag = '<button type="button" class="btn btn-warning font-size-10 p-1 waves-effect waves-light">Waiting</button>';
      } elseif($ls['flag'] == 2){
        $flag = '<button type="button" class="btn btn-danger font-size-10 p-1 waves-effect waves-light">stock habis</button>';
      } elseif($ls['flag'] == 3){
        $flag = '<button type="button" class="btn btn-warning font-size-10 p-1 waves-effect waves-light">stock kurang</button>';
      } else {
        $flag = '<button type="button" class="btn btn-success font-size-10 p-1 waves-effect waves-light">Paid</button>';
      }

      $row[] = $ls['item_name'];
      $row[] = $ls['uom_name'];
      $row[] = $ls['last_stock'];
      $row[] = $ls['quantity'];
      $row[] = format_rupiah($ls['subtotal'] / $ls['quantity']);
      $row[] = format_rupiah($ls['subtotal']);
      $row[] = $flag;
      $row[] = '<i data-id="'. encrypt_url($ls['subitem_id']) .'"  class="mdi mdi-trash-can cursor-pointer font-size-20 text-danger me-1 delete-item-cart"></i>';

      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->transaction->records_total_cart_item(),
      "recordsFiltered" => $this->transaction->records_filter_cart_item(),
      "data" => $data,
    );
    echo json_encode($output);
  }

  public function get_avail_item($itemID){
    $uom = $this->master->get_avail_uom_item($itemID);
    
    $option = '<option value="">Pilih Unit of Measurement</option>';
    foreach($uom as $data){
      $option .= '<option value="'. $data['uom_id']  .'">'. $data['uom_name'] .'</option>';
    }

    if(!empty($uom)){
      $result = array('status'  => true, 'message' => $option);
    } else {
      $result = array('status'  => false, 'message' => $option);
    }
    echo json_encode($result);
  }

  public function add_to_cart(){
    $data = $this->input->post();
    
    $cek_stock = $this->db->query('select * from m_stock where item_id = '.$data["item"].' and uom_id = ' . $data["uom"] . ' and last_stock <> 0 order by input_date asc')->row_array();
    
    $subtotal = ($cek_stock['price'] * $data['qty']);
    $category = array(
      'item_id'       => $data['item'],
      'uom_id'        => $data['uom'],
      'quantity'      => $data['qty'],
      'subitem_id'    => $cek_stock['stock_id'],
      'subtotal'      => $subtotal,
      'ppn'           => (($subtotal * 11 / 100)),
      'user_id'       => $_SESSION['id_login'],
      'purchase_date' => date('Y-m-d H:i:s')
    );

    if($this->db->insert('tbl_purchase', $category)){
      $result = array('status'  => true, 'message' => 'success add to cart');
    } else {
      $result = array('status'  => false, 'message' => 'failed add to cart');
    }
    echo json_encode($result);
  }

  public function delete_item_cart($subitemId=''){
    if($subitemId != ''){
      $this->db->where('subitem_id', decrypt_url($subitemId));
    }
    $this->db->where('user_id', $_SESSION['id_login']);
    $this->db->where('transaction_id', NULL);
    $this->db->delete('tbl_purchase');
  }

  public function get_sum_cart(){
    $summary_cart = $this->db->query('select sum(subtotal) as subtotal from tbl_purchase where transaction_id is null AND user_id = ' . $_SESSION['id_login'])->row_array();
    if($summary_cart['subtotal'] == 0){
      $result = array('status'  => false, 'message' => 'cart empty');
    } else {
      $ppn = (($summary_cart['subtotal'] * 11) / 100);
      $summary = array(
        'subtotal'    => format_rupiah($summary_cart['subtotal']),
        'ppn'         => format_rupiah($ppn),
        'grand_total' => format_rupiah($summary_cart['subtotal'] + $ppn),
      );
      $result = array('status'  => true, 'message' => $summary);
    }
    echo json_encode($result);

  }

  public function purchase(){
    $data = $this->input->post();
    $item_cart = $this->transaction->get_item_cart();
    
    $successPay = 0;
    $stockKurang = 0;

    $invoice = array(
      'payment_type'      => $data['_payment_method'],
      'input_by'          => $_SESSION['id_login'],
      'transaction_date'  => date('Y-m-d H:i:s'), 
    );
    $this->db->insert('tbl_transaction', $invoice);
    $invoiceID = $this->db->insert_id();

    foreach($item_cart as $item){
      $subitem    = $item['subitem_id'];
      $quantity   = $item['quantity'];
      $item_cart  = $this->db->query('select last_stock from m_stock where stock_id = ' . $subitem)->row_array();
      $last_stock = ($item_cart['last_stock'] - $quantity);

      $this->db->where('subitem_id', $subitem);
      $this->db->where('item_id', $item['item_id']);
      $this->db->where('uom_id', $item['uom_id']);
      $this->db->where('(flag is null OR flag != 1)');
      $this->db->where('user_id', $_SESSION['id_login']);
      $this->db->where('transaction_id', NULL);

      if($quantity > $item_cart['last_stock']){
        $this->db->update('tbl_purchase', array('flag' => 3));
        $stockKurang += 1;
      } else {
        // Update transaction
        $this->db->update('tbl_purchase', array('flag' => 1));

        // Update stock
        $this->db->where('stock_id', $subitem);
        $this->db->update('m_stock', array('last_stock' => $last_stock));

        $this->db->where('subitem_id', $subitem);
        $this->db->where('item_id', $item['item_id']);
        $this->db->where('uom_id', $item['uom_id']);
        $this->db->where('flag', 1);
        $this->db->where('user_id', $_SESSION['id_login']);
        $this->db->where('transaction_id', NULL);
        $this->db->update('tbl_purchase', array('transaction_id' => $invoiceID));

        $successPay += 1;
      }
    }

    if($successPay == 0){
      $result = array('status'  => false, 'message' => 'payment_failed');
    } else {
      $invoiceNumber = 'INV/' . bulanromawi(date('m')) . '/' . date('Y') . '/' . str_pad($invoiceID, 3, '0', STR_PAD_LEFT);
      $this->db->where('transaction_id', $invoiceID);
      $this->db->update('tbl_transaction', array('invoice_number' => $invoiceNumber));

      if($successPay > 0 && $stockKurang > 0){
        $result = array('status'  => false, 'message' => 'separated_success');
      } else {
        $result = array('status'  => true, 'message' => 'payment_success');
      }
    }
    echo json_encode($result);
  }

  public function report()
  {
    $data['javascript'] = 'transaction-report.js';
    $this->template->load('template', 'report/transaction', $data);
  }

  public function get_transaction_report()
  {
    $list = $this->transaction->get_transaction_invoice();

    $data = array();
    $no = $_POST['start'];

    foreach ($list as $ls) {
      $no++;
      $row = array();

      $row[] = tanggal_indo($ls['transaction_date'], 'date');
      $row[] = $ls['invoice_number'];
      $row[] = $ls['payment_name'];
      $row[] = '<a target="_blank" href="'. base_url() . 'transaction/invoice-detail/' . encrypt_url($ls['transaction_id']) .'"><i class="mdi mdi-eye cursor-pointer font-size-16 text-default me-1 btn-update-category"></i></a>';
      
      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->transaction->records_total_cart_item(),
      "recordsFiltered" => $this->transaction->records_filter_cart_item(),
      "data" => $data,
    );
    echo json_encode($output);
  }

  public function invoice_detail($invoiceID)
  {
    $data['javascript'] = 'invoice-detail.js';
    $data['invoice'] = $this->transaction->detail_invoice(decrypt_url($invoiceID));
    $this->template->load('template', 'report/invoice-detail', $data);
  }

  public function get_invoice_detail()
  {
    $list = $this->transaction->get_paid_transaction();

    $data = array();
    $no = $_POST['start'];

    foreach ($list as $ls) {
      $no++;
      $row = array();

      $row[] = $ls['item_sku'];
      $row[] = $ls['item_name'];
      $row[] = $ls['quantity'];
      $row[] = $ls['uom_name'];
      $row[] = format_rupiah($ls['subtotal']);

      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->transaction->records_total_paid(),
      "recordsFiltered" => $this->transaction->records_filter_paid(),
      "data" => $data,
    );
    echo json_encode($output);
  }
}