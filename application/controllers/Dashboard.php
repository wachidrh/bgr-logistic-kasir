<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    if (!isset($_SESSION['id_login'])) {
      redirect('login');
    }
    $this->load->model('Model_transaction', 'transaction');
  }

  public function index()
  {
    $data['javascript'] = 'dashboard.js';
    $this->template->load('template', 'dashboard', $data);
  }

  public function get_data_penjualan()
  {
    $list = $this->transaction->get_paid_transaction();

    $data = array();
    $no = $_POST['start'];

    foreach ($list as $ls) {
      $no++;
      $row = array();
     
      $row[] = tanggal_indo($ls['purchase_date'], 'date');
      $row[] = $ls['item_sku'];
      $row[] = $ls['item_name'];
      $row[] = $ls['quantity'];
      $row[] = $ls['uom_name'];
      $row[] = format_rupiah($ls['subtotal']);
      $row[] = format_rupiah($ls['subtotal'] + $ls['ppn']);

      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->transaction->records_total_paid(),
      "recordsFiltered" => $this->transaction->records_filter_paid(),
      "data" => $data,
    );
    echo json_encode($output);
  }

  public function add_category()
  {
    $data = $this->input->post();
    $category = array(
      'category_name' => $data['category_name'],
      'category_desc' => $data['category_desc'],
      'input_date' => date('Y-m-d H:i:s')
    );

    if($this->db->insert('m_category', $category)){
      $result = array('status'  => true, 'message' => 'success add category');
    } else {
      $result = array('status'  => false, 'message' => 'failed add category');
    }
    echo json_encode($result);
  }

  public function flag_category(){
    $data = $this->input->post();

    if(decrypt_url($data['flag']) == 0){
      $flag = 1;
    } else {
      $flag = 0;
    }
    $inactive = $this->db->update('m_category', array('flag' => $flag, 'update_date' => date('Y-m-d H:i:s')), array('category_id' => decrypt_url($data['id'])));
    if($this->db->affected_rows()){
      $result = array('status'  => true, 'message' => 'success update category');
    } else {
      $result = array('status'  => false, 'message' => 'failed update category');
    }
    echo json_encode($result);
  }

  public function get_detail_category(){
    $data = $this->input->post();
    $category = $this->category->detail_category(decrypt_url($data['id']));
    echo json_encode($category);
  }

  public function update_category(){
    $data = $this->input->post();
    $update = $this->db->update('m_category', array('category_name' => $data['_category_name'], 'category_desc' => $data['_category_desc'], 'update_date' => date('Y-m-d H:i:s')), array('category_id' => decrypt_url($data['_category_id'])));
    if($this->db->affected_rows()){
      $result = array('status'  => true, 'message' => 'success update category');
    } else {
      $result = array('status'  => false, 'message' => 'failed update category');
    }
    echo json_encode($result);
  }
}
