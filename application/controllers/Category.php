<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    if (!isset($_SESSION['id_login'])) {
      redirect('login');
    }
    $this->load->model('Model_category', 'category');
  }

  public function index()
  {
    $data['javascript'] = 'master/category.js';
    $this->template->load('template', 'master/category', $data);
  }

  public function get_data_category()
  {
    $list = $this->category->get_data_category();

    $data = array();
    $no = $_POST['start'];

    foreach ($list as $ls) {
      $no++;
      $row = array();
      
      if($ls['flag'] == 1){
        $flag = '<button type="button" class="btn btn-success btn-rounded waves-effect waves-light">Active</button>';
        $btnFlag = '<i data-id="'. encrypt_url($ls['category_id']) .'" data-flag="'. encrypt_url($ls['flag']) .'" title="Mark as Inactive" class="mdi mdi-close cursor-pointer font-size-20 text-danger me-1 btn-inactive-category"></i>';
      } else {
        $flag = '<button type="button" class="btn btn-danger btn-rounded waves-effect waves-light">Inactive</button>';
        $btnFlag = '<i data-id="'. encrypt_url($ls['category_id']) .'" data-flag="'. encrypt_url($ls['flag']) .'" title="Mark as Active" class="mdi mdi-check cursor-pointer font-size-20 text-success me-1 btn-inactive-category"></i>';
      }
      
      $row[] = $ls['category_name'];
      $row[] = $ls['category_desc'] == '' ? '-' : $ls['category_desc'];
      $row[] = $flag;
      $row[] = '<i data-id="'. encrypt_url($ls['category_id']) .'" title="Update Kategori" class="mdi mdi-pencil cursor-pointer font-size-20 text-info me-1 btn-update-category"></i> 
                '. $btnFlag .'
              ';

      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->category->records_total_category(),
      "recordsFiltered" => $this->category->records_filter_category(),
      "data" => $data,
    );
    echo json_encode($output);
  }

  public function add_category()
  {
    $data = $this->input->post();
    $category = array(
      'category_name' => $data['category_name'],
      'category_desc' => $data['category_desc'],
      'input_date' => date('Y-m-d H:i:s')
    );

    if($this->db->insert('m_category', $category)){
      $result = array('status'  => true, 'message' => 'success add category');
    } else {
      $result = array('status'  => false, 'message' => 'failed add category');
    }
    echo json_encode($result);
  }

  public function flag_category(){
    $data = $this->input->post();

    if(decrypt_url($data['flag']) == 0){
      $flag = 1;
    } else {
      $flag = 0;
    }
    $inactive = $this->db->update('m_category', array('flag' => $flag, 'update_date' => date('Y-m-d H:i:s')), array('category_id' => decrypt_url($data['id'])));
    if($this->db->affected_rows()){
      $result = array('status'  => true, 'message' => 'success update category');
    } else {
      $result = array('status'  => false, 'message' => 'failed update category');
    }
    echo json_encode($result);
  }

  public function get_detail_category(){
    $data = $this->input->post();
    $category = $this->category->detail_category(decrypt_url($data['id']));
    echo json_encode($category);
  }

  public function update_category(){
    $data = $this->input->post();
    $update = $this->db->update('m_category', array('category_name' => $data['_category_name'], 'category_desc' => $data['_category_desc'], 'update_date' => date('Y-m-d H:i:s')), array('category_id' => decrypt_url($data['_category_id'])));
    if($this->db->affected_rows()){
      $result = array('status'  => true, 'message' => 'success update category');
    } else {
      $result = array('status'  => false, 'message' => 'failed update category');
    }
    echo json_encode($result);
  }
}
