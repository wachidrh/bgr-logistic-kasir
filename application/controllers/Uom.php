<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Uom extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    if (!isset($_SESSION['id_login'])) {
      redirect('login');
    }
    $this->load->model('Model_uom', 'uom');
  }

  public function index()
  {
    $data['javascript'] = 'master/uom.js';
    $this->template->load('template', 'master/uom', $data);
  }

  public function get_data_uom()
  {
    $list = $this->uom->get_data_uom();

    $data = array();
    $no = $_POST['start'];

    foreach ($list as $ls) {
      $no++;
      $row = array();
      
      if($ls['flag'] == 1){
        $flag = '<button type="button" class="btn btn-success btn-rounded waves-effect waves-light">Active</button>';
        $btnFlag = '<i data-id="'. encrypt_url($ls['uom_id']) .'" data-flag="'. encrypt_url($ls['flag']) .'" title="Mark as Inactive" class="mdi mdi-close cursor-pointer font-size-20 text-danger me-1 btn-inactive-uom"></i>';
      } else {
        $flag = '<button type="button" class="btn btn-danger btn-rounded waves-effect waves-light">Inactive</button>';
        $btnFlag = '<i data-id="'. encrypt_url($ls['uom_id']) .'" data-flag="'. encrypt_url($ls['flag']) .'" title="Mark as Active" class="mdi mdi-check cursor-pointer font-size-20 text-success me-1 btn-inactive-uom"></i>';
      }
      
      $row[] = $ls['uom_name'];
      $row[] = $ls['uom_desc'] == '' ? '-' : $ls['uom_desc'];
      $row[] = $flag;
      $row[] = '<i data-id="'. encrypt_url($ls['uom_id']) .'" title="Update UoM" class="mdi mdi-pencil cursor-pointer font-size-20 text-info me-1 btn-update-uom"></i> 
                '. $btnFlag .'
              ';

      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->uom->records_total_uom(),
      "recordsFiltered" => $this->uom->records_filter_uom(),
      "data" => $data,
    );
    echo json_encode($output);
  }

  public function add_uom()
  {
    $data = $this->input->post();
    $uom = array(
      'uom_name' => $data['uom_name'],
      'uom_desc' => $data['uom_desc'],
      'insert_date' => date('Y-m-d H:i:s')
    );

    if($this->db->insert('m_uom', $uom)){
      $result = array('status'  => true, 'message' => 'success add unit of measurement');
    } else {
      $result = array('status'  => false, 'message' => 'failed add unit of measurement');
    }
    echo json_encode($result);
  }

  public function flag_uom(){
    $data = $this->input->post();

    if(decrypt_url($data['flag']) == 0){
      $flag = 1;
    } else {
      $flag = 0;
    }
    $inactive = $this->db->update('m_uom', array('flag' => $flag, 'update_date' => date('Y-m-d H:i:s')), array('uom_id' => decrypt_url($data['id'])));
    if($this->db->affected_rows()){
      $result = array('status'  => true, 'message' => 'success update unit of measurement');
    } else {
      $result = array('status'  => false, 'message' => 'failed update unit of measurement');
    }
    echo json_encode($result);
  }

  public function get_detail_uom(){
    $data = $this->input->post();
    $uom = $this->uom->detail_uom(decrypt_url($data['id']));
    echo json_encode($uom);
  }

  public function update_uom(){
    $data = $this->input->post();
    $update = $this->db->update('m_uom', array('uom_name' => $data['_uom_name'], 'uom_desc' => $data['_uom_desc'], 'update_date' => date('Y-m-d H:i:s')), array('uom_id' => decrypt_url($data['_uom_id'])));
    if($this->db->affected_rows()){
      $result = array('status'  => true, 'message' => 'success update unit of measurement');
    } else {
      $result = array('status'  => false, 'message' => 'failed update unit of measurement');
    }
    echo json_encode($result);
  }
}
