<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Item extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    if (!isset($_SESSION['id_login'])) {
      redirect('login');
    }
    $this->load->model('Model_item', 'item');
    $this->load->model('Model_master', 'master');
  }

  public function index()
  {
    $data['javascript'] = 'master/item.js';
    $data['category'] = $this->master->get_category();
    $data['uom'] = $this->master->get_uom();

    $this->template->load('template', 'master/item', $data);
  }

  public function get_data_item()
  {
    $list = $this->item->get_data_item();

    $data = array();
    $no = $_POST['start'];

    foreach ($list as $ls) {
      $no++;
      $row = array();
      
      if($ls['flag'] == 1){
        $flag = '<button type="button" class="btn btn-success btn-rounded waves-effect waves-light">Active</button>';
        $btnFlag = '<i data-id="'. encrypt_url($ls['item_id']) .'" data-flag="'. encrypt_url($ls['flag']) .'" title="Mark as Inactive" class="mdi align-middle mdi-close cursor-pointer font-size-20 text-danger me-1 btn-inactive-item"></i>';
      } else {
        $flag = '<button type="button" class="btn btn-danger btn-rounded waves-effect waves-light">Inactive</button>';
        $btnFlag = '<i data-id="'. encrypt_url($ls['item_id']) .'" data-flag="'. encrypt_url($ls['flag']) .'" title="Mark as Active" class="mdi align-middle mdi-check cursor-pointer font-size-20 text-success me-1 btn-inactive-item"></i>';
      }

      $row[] = $ls['item_sku'];
      $row[] = $ls['item_name'];
      // $row[] = $ls['uom_name'];
      $row[] = $ls['category_name'];
      // $row[] = $ls['last_stock'];
      // $row[] = format_rupiah($ls['last_price_stock']);
      $row[] = $flag;
      $row[] = '<a target="_blank" href="'. base_url() . 'item/sub-item/' . encrypt_url($ls['item_id']) . '"><button class="btn btn-success waves-effect font-size-10 p-1 me-2 waves-light" type="button">Stock</button></a>
                <i data-id="'. encrypt_url($ls['item_id']) .'" title="Update Item" class="mdi align-middle mdi-pencil cursor-pointer font-size-20 text-info me-1 btn-update-item"></i> 
                '. $btnFlag .'
              ';

      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->item->records_total_item(),
      "recordsFiltered" => $this->item->records_filter_item(),
      "data" => $data,
    );
    echo json_encode($output);
  }

  public function add_item()
  {
    $data = $this->input->post();
    $item = array(
      'item_sku'        => $data['sku'],
      'item_name'       => $data['item_name'],
      'category_id'     => $data['category'],
      'input_date'      => date('Y-m-d H:i:s')
    );

    if($this->db->insert('m_item', $item)){
      $result = array('status'  => true, 'message' => 'success add item');
    } else {
      $result = array('status'  => false, 'message' => 'failed add item');
    }
    echo json_encode($result);
  }

  public function get_detail_item(){
    $data = $this->input->post();
    $item = $this->item->detail_item(decrypt_url($data['id']));
    echo json_encode($item);
  }

  public function flag_item(){
    $data = $this->input->post();

    if(decrypt_url($data['flag']) == 0){
      $flag = 1;
    } else {
      $flag = 0;
    }
    $inactive = $this->db->update('m_item', array('flag' => $flag, 'update_date' => date('Y-m-d H:i:s')), array('item_id' => decrypt_url($data['id'])));
    if($this->db->affected_rows()){
      $result = array('status'  => true, 'message' => 'success update unit of measurement');
    } else {
      $result = array('status'  => false, 'message' => 'failed update unit of measurement');
    }
    echo json_encode($result);
  }

  public function update_item(){
    $data = $this->input->post();

    $item = array(
      'item_sku'        => $data['_sku'],
      'item_name'       => $data['_item_name'],
      'category_id'     => $data['_category'],
      'update_date'     => date('Y-m-d H:i:s')
    );

    $update = $this->db->update('m_item', $item, array('item_id' => decrypt_url($data['_item_id'])));
    if($this->db->affected_rows()){
      $result = array('status'  => true, 'message' => 'success update unit of measurement');
    } else {
      $result = array('status'  => false, 'message' => 'failed update unit of measurement');
    }
    echo json_encode($result);
  }

  public function sub_item($itemID)
  {
    $data['javascript'] = 'master/sub-item.js';
    $data['uom']  = $this->master->get_uom();
    $data['item'] = $this->item->detail_item(decrypt_url($itemID));

    $this->template->load('template', 'master/sub-item', $data);
  }

  public function data_sub_item($itemID)
  {
    $itemID = decrypt_url($itemID);
    $list = $this->item->get_data_sub_item($itemID);
   
    $data = array();
    $no = $_POST['start'];

    foreach ($list as $ls) {
      $no++;
      $row = array();
      

      $row[] = $ls['item_name'];
      $row[] = $ls['uom_name'];
      $row[] = $ls['total_stock'];
      $row[] = $ls['last_stock'];
      $row[] = format_rupiah($ls['price']);
      $row[] = tanggal_indo($ls['input_date'], 'datetime');

      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->item->records_total_sub_item($itemID),
      "recordsFiltered" => $this->item->records_filter_sub_item($itemID),
      "data" => $data,
    );
    echo json_encode($output);
  }

  public function add_sub_item()
  {
    $data = $this->input->post();
    $item = array(
      'item_id'         => decrypt_url($data['_item_id']),
      'uom_id'          => $data['_sub_uom'],
      'total_stock'     => $data['_sub_stock'],
      'last_stock'      => $data['_sub_stock'],
      'price'           => preg_replace('~[Rp.]~', '', $data['_sub_price']),
      'input_date'      => date('Y-m-d H:i:s')
    );

    if($this->db->insert('m_stock', $item)){
      $result = array('status'  => true, 'message' => 'success add sub item');
    } else {
      $result = array('status'  => false, 'message' => 'failed add sub item');
    }
    echo json_encode($result);
  }

  public function report_stock(){
    $data['javascript'] = 'report-stock.js';
    $this->template->load('template', 'report/stock', $data);
  }


  public function get_report_stock()
  {
    $list = $this->item->data_report_stock();

    $data = array();
    $no = $_POST['start'];

    foreach ($list as $ls) {
      $no++;
      $row = array();
      
      $row[] = $ls['item_sku'];
      $row[] = $ls['item_name'];
      $row[] = $ls['uom_name'];
      $row[] = $ls['last_stock'];

      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->item->records_total_item(),
      "recordsFiltered" => $this->item->records_filter_item(),
      "data" => $data,
    );
    echo json_encode($output);
  }
}
