<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payment extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    if (!isset($_SESSION['id_login'])) {
      redirect('login');
    }
    $this->load->model('Model_payment', 'payment');
  }

  public function index()
  {
    $data['javascript'] = 'master/payment.js';
    $this->template->load('template', 'master/payment', $data);
  }

  public function get_data_payment()
  {
    $list = $this->payment->get_data_payment();

    $data = array();
    $no = $_POST['start'];

    foreach ($list as $ls) {
      $no++;
      $row = array();
      
      if($ls['flag'] == 1){
        $flag = '<button type="button" class="btn btn-success btn-rounded waves-effect waves-light">Active</button>';
        $btnFlag = '<i data-id="'. encrypt_url($ls['payment_id']) .'" data-flag="'. encrypt_url($ls['flag']) .'" title="Mark as Inactive" class="mdi mdi-close cursor-pointer font-size-20 text-danger me-1 btn-inactive-payment"></i>';
      } else {
        $flag = '<button type="button" class="btn btn-danger btn-rounded waves-effect waves-light">Inactive</button>';
        $btnFlag = '<i data-id="'. encrypt_url($ls['payment_id']) .'" data-flag="'. encrypt_url($ls['flag']) .'" title="Mark as Active" class="mdi mdi-check cursor-pointer font-size-20 text-success me-1 btn-inactive-payment"></i>';
      }
      
      $row[] = $ls['payment_name'];
      $row[] = $ls['payment_desc'] == '' ? '-' : $ls['payment_desc'];
      $row[] = $flag;
      $row[] = '<i data-id="'. encrypt_url($ls['payment_id']) .'" title="Update payment" class="mdi mdi-pencil cursor-pointer font-size-20 text-info me-1 btn-update-payment"></i> 
                '. $btnFlag .'
              ';

      $data[] = $row;
    }

    $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->payment->records_total_payment(),
      "recordsFiltered" => $this->payment->records_filter_payment(),
      "data" => $data,
    );
    echo json_encode($output);
  }

  public function add_payment()
  {
    $data = $this->input->post();
    $payment = array(
      'payment_name' => $data['payment_name'],
      'payment_desc' => $data['payment_desc'],
      'insert_date' => date('Y-m-d H:i:s')
    );

    if($this->db->insert('m_payment_method', $payment)){
      $result = array('status'  => true, 'message' => 'success add payment type');
    } else {
      $result = array('status'  => false, 'message' => 'failed add payment type');
    }
    echo json_encode($result);
  }

  public function flag_payment(){
    $data = $this->input->post();

    if(decrypt_url($data['flag']) == 0){
      $flag = 1;
    } else {
      $flag = 0;
    }
    $inactive = $this->db->update('m_payment_method', array('flag' => $flag, 'update_date' => date('Y-m-d H:i:s')), array('payment_id' => decrypt_url($data['id'])));
    if($this->db->affected_rows()){
      $result = array('status'  => true, 'message' => 'success update payment type');
    } else {
      $result = array('status'  => false, 'message' => 'failed update payment type');
    }
    echo json_encode($result);
  }

  public function get_detail_payment(){
    $data = $this->input->post();
    $payment = $this->payment->detail_payment(decrypt_url($data['id']));
    echo json_encode($payment);
  }

  public function update_payment(){
    $data = $this->input->post();
    $update = $this->db->update('m_payment_method', array('payment_name' => $data['_payment_name'], 'payment_desc' => $data['_payment_desc'], 'update_date' => date('Y-m-d H:i:s')), array('payment_id' => decrypt_url($data['_payment_id'])));
    if($this->db->affected_rows()){
      $result = array('status'  => true, 'message' => 'success update payment type');
    } else {
      $result = array('status'  => false, 'message' => 'failed update payment type');
    }
    echo json_encode($result);
  }
}
