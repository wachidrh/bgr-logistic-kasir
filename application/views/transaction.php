<div class="page-content">
	<div class="container-fluid">

		<!-- start page title -->
		<div class="row">
			<div class="col-12">
				<div class="page-title-box d-sm-flex align-items-center justify-content-between">
					<h4 class="mb-sm-0 font-size-18">Transaksi</h4>
					<div class="page-title-right">
						<ol class="breadcrumb m-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url() . 'transaction'; ?>">Transaksi</a></li>
						</ol>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<form id="form_transaction" method="post">
							<div class="col-lg-12 mb-2">
								<label class="form-label">Item</label>
								<select name="item" id="item" class="form-control select2">
									<option value="">Pilih item</option>
									<?php foreach ($item as $ls) { ?>
										<option value="<?php echo $ls['item_id'] ?>"><?php echo $ls['item_name'] ?> (<?php echo $ls['item_sku'] ?>)</option>
									<?php } ?>
								</select>
								<small id="err_item"></small>
							</div>
							<div class="col-lg-12 mb-2">
								<label class="form-label">Unit of Measurement</label>
								<select name="uom" id="uom" class="form-control select2" disabled='true'>
									<option value="">Pilih Unit of Measurement</option>
								</select>
								<small id="err_uom"></small>
							</div>
							<div class="col-lg-12 mb-2">
								<label class="form-label">Quantity</label>
								<input class="form-control" type="number" name="qty" id="qty">
							</div>
							<div class="mt-3">
								<button type="submit" class="btn btn-primary waves-effect waves-light">Add to Cart</button>
								<div class="d-flex flex-wrap gap-2 float-end">
									<button type="button" id="clear-cart" class="btn btn-danger waves-effect waves-light">Clear Cart</button>
									<button type="button" id="summary-transaction" class="btn btn-success waves-effect waves-light">Bayar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table id="table_cart" class="table align-middle table-nowrap table-check">
								<thead class="table-light">
									<tr>
										<th class="align-middle">Item name</th>
										<th class="align-middle">UoM</th>
										<th class="align-middle text-center">Stock Terakhir</th>
										<th class="align-middle text-center">Quantity</th>
										<th class="align-middle">Harga</th>
										<th class="align-middle">Sub total</th>
										<th class="align-middle">Status</th>
										<th class="align-middle text-center">Action</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end row -->
	</div> <!-- container-fluid -->
</div>
<!-- End Page-content -->

<!-- Modal Pay -->
<div id="modalPay" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myModalLabel">Rincian Transaksi</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form id="form_pay_now">
				<div class="modal-body">
					<div class="row">
						<label class="col-md-3 col-form-label pb-1">Sub Total</label>
						<div class="col-md-9 text-end">
							<label class="col-form-label pb-1 fw-bold" id="_subtotal">Rp. 0</label>
						</div>
					</div>
					<div class="row">
						<label class="col-md-3 col-form-label pb-1">PPN 11%</label>
						<div class="col-md-9 text-end">
							<label class="col-form-label pb-1 fw-bold" id="_ppn">Rp. 0</label>
						</div>
					</div>
					<div class="row mb-2">
						<label class="col-md-3 col-form-label pb-1">Grand Total</label>
						<div class="col-md-9 text-end">
							<label class="col-form-label pb-1 fw-bold" id="_grand_total">Rp. 0</label>
						</div>
					</div>
					<div class="col-lg-12 mb-2">
						<label class="form-label">Metode Pembayaran</label>
						<select name="_payment_method" id="_payment_method" class="form-control select2">
							<option value="">Pilih metode pembayaran</option>
							<?php foreach($payment as $ls) { ?>
								<option value="<?php echo $ls['payment_id'] ?>"><?php echo $ls['payment_name'] ?></option>
							<?php } ?>
						</select>
						<small id="_err_payment"></small>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-success waves-effect waves-light">Bayar Sekarang</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->