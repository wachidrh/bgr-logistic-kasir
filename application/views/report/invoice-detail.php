<div class="page-content">
	<div class="container-fluid">

		<!-- start page title -->
		<div class="row">
			<div class="col-12">
				<div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <input type="hidden" id="invoice-id" value="<?php echo $this->uri->segment(3); ?>">
					<h4 class="mb-sm-0 font-size-18">Detail Invoice</h4>
					<div class="page-title-right">
						<ol class="breadcrumb m-0">
							<li class="breadcrumb-item">Transaction</li>
							<li class="breadcrumb-item">Invoice</li>
                            <li class="breadcrumb-item active">Detail Invoice</li>
						</ol>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
                <div class="card">
					<div class="card-body">
						<div class="row mb-2">
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Invoice Number</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" value="<?php echo $invoice['invoice_number']; ?>" disabled>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Tanggal Pembelian</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" value="<?php echo tanggal_indo($invoice['transaction_date'], 'datetime'); ?>" disabled>
                                </div>
                            </div>
							<div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Metode Pembayaran</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" value="<?php echo $invoice['payment_name']; ?>" disabled>
                                </div>
                            </div>
							<div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Kasir</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" value="<?php echo $invoice['fullname']; ?>" disabled>
                                </div>
                            </div>
                        </div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="row mb-2">
                            <div class="col-sm-12">
                                <div class="search-box me-2 mb-2 d-inline-block">
                                    <div class="position-relative">
                                        <input type="text" id="search_item" class="form-control" placeholder="Cari">
                                        <i class="bx bx-search-alt search-icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="master_sub_item" class="table align-middle table-nowrap table-check">
                                <thead class="table-light">
                                    <tr>
                                        <th class="align-middle">SKU</th>
                                        <th class="align-middle">Nama Item</th>
                                        <th class="align-middle text-center">Qty</th>
                                        <th class="align-middle text-center">UoM</th>
                                        <th class="align-middle">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

					</div>
				</div>
			</div>
		</div>
		<!-- end row -->
	</div> <!-- container-fluid -->
</div>
<!-- End Page-content -->