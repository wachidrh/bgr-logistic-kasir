<div class="page-content">
	<div class="container-fluid">

		<!-- start page title -->
		<div class="row">
			<div class="col-12">
				<div class="page-title-box d-sm-flex align-items-center justify-content-between">
                    <input type="hidden" id="item-id" value="<?php echo $this->uri->segment(3); ?>">
					<h4 class="mb-sm-0 font-size-18">Master Sub Item</h4>
					<div class="page-title-right">
						<ol class="breadcrumb m-0">
							<li class="breadcrumb-item">Data Master</li>
							<li class="breadcrumb-item">Item</li>
                            <li class="breadcrumb-item active">Sub Item</li>
						</ol>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
                <div class="card">
					<div class="card-body">
						<div class="row mb-2">
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">SKU</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" value="<?php echo $item['item_sku']; ?>" disabled>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="example-text-input" class="col-md-2 col-form-label">Nama Item</label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" value="<?php echo $item['item_name']; ?>" disabled>
                                </div>
                            </div>
                        </div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="row mb-2">
                            <div class="col-sm-4">
                                <div class="search-box me-2 mb-2 d-inline-block">
                                    <div class="position-relative">
                                        <input type="text" id="search_sub_item" class="form-control" placeholder="Cari">
                                        <i class="bx bx-search-alt search-icon"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-sm-end">
                                    <button type="button" id="btn-add-sub-item" class="btn btn-success btn-rounded waves-effect waves-light mb-2 me-2" data-bs-toggle="modal" data-bs-target="#modalAddSubItem"><i class="mdi mdi-plus me-1"></i> Tambah Data Sub Item</button>
                                </div>
                            </div><!-- end col-->
                        </div>
                        <div class="table-responsive">
                            <table id="master_sub_item" class="table align-middle table-nowrap table-check">
                                <thead class="table-light">
                                    <tr>
                                        <th class="align-middle">Nama Item</th>
                                        <th class="align-middle text-center">UoM</th>
                                        <th class="align-middle text-center">Stock</th>
                                        <th class="align-middle text-center">Last Stock</th>
                                        <th class="align-middle text-center">Harga Jual</th>
                                        <th class="align-middle text-center">Tanggal Input</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

					</div>
				</div>
			</div>
		</div>
		<!-- end row -->
	</div> <!-- container-fluid -->
</div>
<!-- End Page-content -->


<!-- Modal Add Sub Item -->
<div id="modalAddSubItem" class="modal fade" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myModalLabel">Tambah Data Sub Item</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form id="form_sub_item" method="post">
				<div class="modal-body">
                    <input class="form-control" type="hidden" name="_item_id" id="_item_id" value="<?php echo $this->uri->segment(3); ?>">
                    <div class="col-lg-12 mb-2">
						<label class="form-label">Unit of Measurement (UoM)</label>
						<select name="_sub_uom" id="_sub_uom" class="form-control select2">
							<option value="">Pilih Unit of Measurement</option>
							<?php foreach ($uom as $ls) { ?>
								<option value="<?php echo $ls['uom_id'] ?>"><?php echo $ls['uom_name'] ?></option>
							<?php } ?>
						</select>
						<small id="err_sub_uom"></small>
					</div>
                    <div class="col-lg-12 mb-2">
						<label class="form-label">Stock</label>
						<input class="form-control" type="number" name="_sub_stock" id="_sub_stock">
					</div>
                    <div class="col-lg-12 mb-2">
						<label class="form-label">Harga Jual</label>
						<input class="form-control" type="text" name="_sub_price" id="_sub_price">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-primary waves-effect waves-light">Simpan</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->