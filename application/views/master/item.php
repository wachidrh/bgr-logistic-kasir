<div class="page-content">
	<div class="container-fluid">

		<!-- start page title -->
		<div class="row">
			<div class="col-12">
				<div class="page-title-box d-sm-flex align-items-center justify-content-between">
					<h4 class="mb-sm-0 font-size-18">Master Item</h4>
					<div class="page-title-right">
						<ol class="breadcrumb m-0">
							<li class="breadcrumb-item">Data Master</li>
							<li class="breadcrumb-item active">Item</li>
						</ol>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row mb-2">
							<div class="col-sm-4">
								<div class="search-box me-2 mb-2 d-inline-block">
									<div class="position-relative">
										<input type="text" id="search_item" class="form-control" placeholder="Cari">
										<i class="bx bx-search-alt search-icon"></i>
									</div>
								</div>
							</div>
							<div class="col-sm-8">
								<div class="text-sm-end">
									<button type="button" id="btn-add-uom" class="btn btn-success btn-rounded waves-effect waves-light mb-2 me-2" data-bs-toggle="modal" data-bs-target="#modalAddItem"><i class="mdi mdi-plus me-1"></i> Tambah Data Item</button>
								</div>
							</div><!-- end col-->
						</div>

						<div class="table-responsive">
							<table id="master_item" class="table align-middle table-nowrap table-check">
								<thead class="table-light">
									<tr>
										<th class="align-middle">SKU</th>
										<th class="align-middle">Nama Item</th>
										<th class="align-middle text-center">Kategori</th>
                                        <th class="align-middle text-center">Status</th>
                                        <th class="align-middle text-center">Action</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- end row -->
	</div> <!-- container-fluid -->
</div>
<!-- End Page-content -->

<!-- Modal Add Item -->
<div id="modalAddItem" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myModalLabel">Tambah Data Item</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form id="form_item" method="post">
				<div class="modal-body">
					<div class="col-lg-12 mb-2">
						<label class="form-label">SKU</label>
						<input class="form-control" type="text" name="sku" id="sku">
					</div>
                    <div class="col-lg-12 mb-2">
						<label class="form-label">Kategori</label>
						<select name="category" id="category" class="form-control select2">
							<option value="">Pilih kategori</option>
							<?php foreach ($category as $ls) { ?>
								<option value="<?php echo $ls['category_id'] ?>"><?php echo $ls['category_name'] ?></option>
							<?php } ?>
						</select>
						<small id="_err_category"></small>
					</div>
					<div class="col-lg-12 mb-2">
						<label class="form-label">Nama item</label>
						<input class="form-control" type="text" name="item_name" id="item_name">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-primary waves-effect waves-light">Simpan</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal Update Item  -->
<div id="modalUpdateItem" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myModalLabel">Update Item</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form id="form_update_item" method="post">
				<div class="modal-body">
					<input class="form-control" type="hidden" name="_item_id" id="_item_id">
					<div class="col-lg-12 mb-2">
						<label class="form-label">SKU</label>
						<input class="form-control" type="text" name="_sku" id="_sku">
					</div>
                    <div class="col-lg-12 mb-2">
						<label class="form-label">Kategori</label>
						<select name="_category" id="_category" class="form-control select2">
							<option value="">Pilih kategori</option>
							<?php foreach($category as $ls) { ?>
								<option value="<?php echo $ls['category_id'] ?>"><?php echo $ls['category_name'] ?></option>
							<?php } ?>
						</select>
						<small id="err_category"></small>
					</div>
					<div class="col-lg-12 mb-2">
						<label class="form-label">Nama item</label>
						<input class="form-control" type="text" name="_item_name" id="_item_name">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-primary waves-effect waves-light">Simpan</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
