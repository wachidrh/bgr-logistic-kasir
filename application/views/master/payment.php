<div class="page-content">
	<div class="container-fluid">

		<!-- start page title -->
		<div class="row">
			<div class="col-12">
				<div class="page-title-box d-sm-flex align-items-center justify-content-between">
					<h4 class="mb-sm-0 font-size-18">Master Payment Type</h4>

					<div class="page-title-right">
						<ol class="breadcrumb m-0">
							<li class="breadcrumb-item">Data Master</li>
							<li class="breadcrumb-item active">Payment type</li>
						</ol>
					</div>

				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row mb-2">
							<div class="col-sm-4">
								<div class="search-box me-2 mb-2 d-inline-block">
									<div class="position-relative">
										<input type="text" id="search_payment" class="form-control" placeholder="Cari">
										<i class="bx bx-search-alt search-icon"></i>
									</div>
								</div>
							</div>
							<div class="col-sm-8">
								<div class="text-sm-end">
									<button type="button" id="btn-add-payment" class="btn btn-success btn-rounded waves-effect waves-light mb-2 me-2" data-bs-toggle="modal" data-bs-target="#modalAddpayment"><i class="mdi mdi-plus me-1"></i> Tambah Data payment</button>
								</div>
							</div><!-- end col-->
						</div>

						<div class="table-responsive">
							<table id="master_payment" class="table align-middle table-nowrap table-check">
								<thead class="table-light">
									<tr>
										<th class="align-middle">Metode Pembayaran</th>
										<th class="align-middle">Deskripsi</th>
										<th class="align-middle text-center">Status</th>
										<th class="align-middle text-center">Action</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- end row -->
	</div> <!-- container-fluid -->
</div>
<!-- End Page-content -->

<!-- Modal Add Payment -->
<div id="modalAddpayment" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myModalLabel">Tambah Data Metode Pembayaran</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form id="form_payment" method="post">
				<div class="modal-body">
					<div class="col-lg-12 mb-2">
						<label class="form-label">Nama metode pembayaran</label>
						<input class="form-control" type="text" name="payment_name" id="payment_name">
					</div>
					<div class="col-lg-12 mb-2">
						<label class="form-label">Deskripsi</label>
						<textarea class="form-control" type="text" name="payment_desc" id="payment_desc"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Batal</button>
					<button type="submit" id="btn-save-payment" class="btn btn-primary waves-effect waves-light">Simpan</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal Add Payment -->
<div id="modalUpdatepayment" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myModalLabel">Update Data Metode Pembayaran</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form id="form_update_payment" method="post">
				<div class="modal-body">
					<input class="form-control" type="hidden" name="_payment_id" id="_payment_id">
					<div class="col-lg-12 mb-2">
						<label class="form-label">Nama metode pembayaran</label>
						<input class="form-control" type="text" name="_payment_name" id="_payment_name">
					</div>
					<div class="col-lg-12 mb-2">
						<label class="form-label">Deskripsi</label>
						<textarea class="form-control" type="text" name="_payment_desc" id="_payment_desc"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Batal</button>
					<button type="submit" id="btn-update-payment" class="btn btn-primary waves-effect waves-light">Update</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->