<div class="page-content">
	<div class="container-fluid">
		<?php 
			if($_SESSION['user_type'] == 1){
				$usertype = 'Super Administrator';
			} elseif($_SESSION['user_type'] == 2){
				$usertype = 'Kasir';
			} else {
				$usertype = 'Admin Stock';
			}
		?>
		<h4 class="font-size-20 mb-3">Selamat datang <?php echo $_SESSION['fullname'] . ' (' . $usertype . ')' ?></h4>
		<!-- start page title -->
		<div class="row">
			<div class="col-12">
				<div class="page-title-box d-sm-flex align-items-center justify-content-between">
					<h5 class="mb-sm-0 font-size-16">Dashboard - Report Penjualan</h5>

					<div class="page-title-right">
						<ol class="breadcrumb m-0">
							<li class="breadcrumb-item"><a href="<?php echo base_url() . 'dashboard'; ?>">Dashboard</a></li>
						</ol>
					</div>

				</div>
			</div>
		</div>
		<!-- end page title -->

		<div class="row">
			<div class="col-xl-12">
				<div class="card">
					<div class="card-body">
						<div class="row mb-2">
							<div class="col-sm-4">
								<div class="search-box me-2 mb-2 d-inline-block">
									<div class="position-relative">
										<input type="text" id="search_item" class="form-control" placeholder="Cari">
										<i class="bx bx-search-alt search-icon"></i>
									</div>
								</div>
							</div>
							<div class="col-sm-8">
								<div class="text-sm-end">
									<button type="button" id="btn-filter" class="btn btn-light btn-rounded waves-effect waves-light mb-2 me-2" data-bs-toggle="modal" data-bs-target="#modalFilter"><i class="mdi mdi-filter me-1"></i> Filter Tanggal</button>
								</div>
							</div><!-- end col-->
						</div>
						<div class="table-responsive">
							<table id="table_dashboard" class="table align-middle table-nowrap table-check">
								<thead class="table-light">
									<tr>
										<th class="align-middle">Tgl Penjualan</th>
										<th class="align-middle">SKU</th>
										<th class="align-middle">Item</th>
                                        <th class="align-middle text-center">Qty</th>
                                        <th class="align-middle text-center">UoM</th>
										<th class="align-middle">Subtotal</th>
										<th class="align-middle">Subtotal + PPN</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div> <!-- end col -->
		</div>
		<!-- end row -->
	</div> <!-- container-fluid -->
</div>
<!-- End Page-content -->

<!-- Modal Add Unit of Measurement -->
<div id="modalFilter" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="myModalLabel">Filter Penjualan</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<div class="mb-4">
					<label>Date Range</label>
					<div class="input-daterange input-group" id="_filter_date" data-date-format="dd/mm/yyyy" data-date-autoclose="true" data-provide="datepicker" data-date-container="#_filter_date">
						<input type="text" class="form-control" name="startDate" id="startDate"  placeholder="Start Date">
						<input type="text" class="form-control" name="endDate" id="endDate"  placeholder="End Date">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Tutup</button>
				<button type="button" id="btn-filter-date" class="btn btn-primary waves-effect waves-light">Filter</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->