<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['logout'] = 'login/logout';

$route['dashboard/get-data-transaction'] = 'dashboard/get_data_penjualan';

// Unit of Measurement
$route['uom/get-data-uom'] = 'uom/get_data_uom';
$route['uom/add-uom'] = 'uom/add_uom';
$route['uom/update-flag-uom'] = 'uom/flag_uom';
$route['uom/get-detail-uom'] = 'uom/get_detail_uom';
$route['uom/update-uom'] = 'uom/update_uom';

// Category
$route['category/get-data-category'] = 'category/get_data_category';
$route['category/add-category'] = 'category/add_category';
$route['category/update-flag-category'] = 'category/flag_category';
$route['category/get-detail-category'] = 'category/get_detail_category';
$route['category/update-category'] = 'category/update_category';

// Payment type
$route['payment/get-data-payment'] = 'payment/get_data_payment';
$route['payment/add-payment'] = 'payment/add_payment';
$route['payment/update-flag-payment'] = 'payment/flag_payment';
$route['payment/get-detail-payment'] = 'payment/get_detail_payment';
$route['payment/update-payment'] = 'payment/update_payment';

// Item
$route['item/get-data-item'] = 'item/get_data_item';
$route['item/add-item'] = 'item/add_item';
$route['item/update-flag-item'] = 'item/flag_item';
$route['item/get-detail-item'] = 'item/get_detail_item';
$route['item/update-item'] = 'item/update_item';
$route['item/report-stock'] = 'item/report_stock';
$route['item/get-report-stock'] = 'item/get_report_stock';

// Sub Item
$route['item/data-sub-item/(:any)'] = 'item/data_sub_item/$1';
$route['item/add-sub-item'] = 'item/add_sub_item';
$route['item/sub-item/(:any)'] = 'item/sub_item/$1';

// Transaction
$route['transaction/get-avail-item/(:any)'] = 'transaction/get_avail_item/$1';
$route['transaction/get-cart-item'] = 'transaction/get_cart_item';
$route['transaction/add-to-cart'] = 'transaction/add_to_cart';
$route['transaction/delete-item-cart'] = 'transaction/delete_item_cart';
$route['transaction/delete-item-cart/(:any)'] = 'transaction/delete_item_cart/$1';
$route['transaction/summary-item-cart'] = 'transaction/get_sum_cart';
$route['transaction/get-transaction-report'] = 'transaction/get_transaction_report';
$route['transaction/invoice-detail/(:any)'] = 'transaction/invoice_detail/$1';
$route['transaction/get-invoice-detail/(:any)'] = 'transaction/get_invoice_detail/$1';

// User access
$route['user-access'] = 'user/index';
$route['user-access/detail/(:any)'] = 'user/detail/$1';
$route['user-access/add-access'] = 'user/add_access';
$route['user-access/update-access'] = 'user/update_access';
$route['user-access/check-account'] = 'user/check_account';



