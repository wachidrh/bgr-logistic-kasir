<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_master extends CI_Model {

    public function __construct() { 
        parent::__construct();
    }

    public function get_item(){
        $this->db->select('*');
        $this->db->from('m_item');
        $this->db->order_by('category_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_available_item(){
        $this->db->select('*');
        $this->db->from('m_item');
        $this->db->where('item_id in (select item_id from m_stock where total_stock <> 0)');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_category(){
        $this->db->select('*');
        $this->db->from('m_category');
        $this->db->where('flag', 1);
        $this->db->order_by('category_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_uom(){
        $this->db->select('*');
        $this->db->from('m_uom');
        $this->db->where('flag', 1);
        $this->db->order_by('uom_id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_avail_uom_item($itemId){
        $this->db->select('a.uom_id, b.uom_name');
        $this->db->from('m_stock a');
        $this->db->join('m_uom b', 'a.uom_id = b.uom_id', 'left');
        $this->db->where('a.item_id', $itemId);
        $this->db->where('a.last_stock !=', 0);
        $this->db->group_by('a.uom_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_payment(){
        $this->db->select('*');
        $this->db->from('m_payment_method');
        $this->db->where('flag', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_user(){
        $data = $this->db->get('tbl_login');
        return $data->result_array();
    }


    public function get_userid($id){
        $data = $this->db->get_where('tbl_login',array('id_login' => $id));
        return $data->row_array();
    }

    public function get_menu(){
        $this->db->order_by('orderkey','asc');
        $data = $this->db->get_where('tbl_menu', array('flag' => 1));
        return $data->result_array();
    }

    public function get_submenu($id){
        $this->db->order_by('orderkey','asc');
        $data = $this->db->get_where('tbl_submenu',array('flag' => 1, 'menu_id' => $id));
        return $data->result_array();
    }

    public function get_menuid($id){
        $this->db->select('menu_id');
        $data = $this->db->get_where('tbl_submenu',array('flag' => 1, 'submenu_id' => $id));
        $hasil = $data->row_array();
        return $hasil['menu_id'];
    }

    public function get_access($id){
        $data = $this->db->get_where('tbl_access',array('id_login' => $id));
        return $data->result_array();
    }

    public function get_accessmenu($id){
        $this->db->group_by('menu_id');
        $this->db->select('menu_id');
        $data = $this->db->get_where('tbl_access',array('id_login' => $id));
        return $data->result_array();
    }
}
