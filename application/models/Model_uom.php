<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_uom extends CI_Model
{
    var $search_uom = array('uom_name', 'uom_desc');
    var $order_uom = array('uom_name', 'uom_desc', 'flag');

    public function _get_data_uom(){
        $this->db->select('*');
        $this->db->from('m_uom');

        $i = 0;
        if (isset($_POST['search']) and !empty($_POST['search'])) {
            foreach ($this->search_uom as $item) {
                if (($_POST['search'])) {
                    if ($i === 0) {
                        $this->db->group_start();
                        $this->db->like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    } else {
                        $this->db->or_like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    }

                    if (count($this->search_uom) - 1 == $i)
                        $this->db->group_end();
                }
                $i++;
            }
        }
        if (isset($_POST['order'])) {
            $this->db->order_by($this->order_uom[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $order = array('uom_id' => 'desc');
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_data_uom(){
        $this->_get_data_uom();
        if ($_POST['length'] != -1) {
            $this->db->limit(10, $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function records_filter_uom(){
        $this->_get_data_uom();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function records_total_uom(){
        $this->db->select('*');
        $this->db->from('m_uom');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function detail_uom($uomId){
        $this->db->select('*');
        $data = $this->db->get_where('m_uom', array('uom_id' => $uomId));
        return $data->row_array();
    }
}
