<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_item extends CI_Model
{
    var $search_item = array('item_name', 'item_sku');
    var $order_item= array('item_sku', 'item_name');

    var $search_sub_item = array('a.total_stock', 'a.last_stock', 'a.price', 'c.uom_name');
    var $order_sub_item= array('stock_id');

    var $search_item_stock = array('b.item_sku', 'b.item_name');

    public function _get_data_item(){
        $this->db->select('a.*,c.category_name');
        $this->db->from('m_item a');
        $this->db->join('m_category c', 'a.category_id = c.category_id', 'left');

        $i = 0;
        if (isset($_POST['search']) and !empty($_POST['search'])) {
            foreach ($this->search_item as $item) {
                if (($_POST['search'])) {
                    if ($i === 0) {
                        $this->db->group_start();
                        $this->db->like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    } else {
                        $this->db->or_like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    }

                    if (count($this->search_item) - 1 == $i)
                        $this->db->group_end();
                }
                $i++;
            }
        }
        if (isset($_POST['order'])) {
            $this->db->order_by($this->order_item[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $order = array('item_id' => 'desc');
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_data_item(){
        $this->_get_data_item();
        if ($_POST['length'] != -1) {
            $this->db->limit(10, $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function records_filter_item(){
        $this->_get_data_item();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function records_total_item(){
        $this->db->select('*');
        $this->db->from('m_item');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function detail_item($itemId){
        $this->db->select('*');
        $data = $this->db->get_where('m_item', array('item_id' => $itemId));
        return $data->row_array();
    }

    // Sub Item
    public function _get_data_sub_item($itemID){
        $this->db->select('a.*,b.item_name,c.uom_name');
        $this->db->from('m_stock a');
        $this->db->join('m_item b', 'a.item_id = b.item_id', 'left');
        $this->db->join('m_uom c', 'a.uom_id = c.uom_id', 'left');
        $this->db->where('a.item_id', $itemID);
        
        $i = 0;
        if (isset($_POST['search']) and !empty($_POST['search'])) {
            foreach ($this->search_sub_item as $item) {
                if (($_POST['search'])) {
                    if ($i === 0) {
                        $this->db->group_start();
                        $this->db->like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    } else {
                        $this->db->or_like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    }

                    if (count($this->search_sub_item) - 1 == $i)
                        $this->db->group_end();
                }
                $i++;
            }
        }
        if (isset($_POST['order'])) {
            $this->db->order_by($this->order_sub_item[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $order = array('a.total_stock' => 'desc', 'a.input_date' => 'asc');
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_data_sub_item($itemID){
        $this->_get_data_sub_item($itemID);
        if ($_POST['length'] != -1) {
            $this->db->limit(10, $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function records_filter_sub_item($itemID){
        $this->_get_data_sub_item($itemID);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function records_total_sub_item($itemID){
        $this->db->select('*');
        $this->db->from('m_stock');
        $this->db->where('item_id', $itemID);
        $query = $this->db->get();
        return $query->num_rows();
    }

    // Report stock
    public function _data_report_stock(){
        $this->db->select('sum(a.total_stock) as total_stock,sum(a.last_stock) as last_stock,b.item_sku,b.item_name,c.uom_name ');
        $this->db->from('m_stock a');
        $this->db->join('m_item b', 'a.item_id = b.item_id', 'left');
        $this->db->join('m_uom c', 'a.uom_id = c.uom_id', 'left');
        $this->db->group_by('a.item_id, a.uom_id');
        
        $i = 0;
        if (isset($_POST['search']) and !empty($_POST['search'])) {
            foreach ($this->search_item_stock as $item) {
                if (($_POST['search'])) {
                    if ($i === 0) {
                        $this->db->group_start();
                        $this->db->like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    } else {
                        $this->db->or_like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    }

                    if (count($this->search_item_stock) - 1 == $i)
                        $this->db->group_end();
                }
                $i++;
            }
        }
        
        // if (isset($_POST['order'])) {
        //     $this->db->order_by($this->order_sub_item[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        // } else {
        //     $order = array('a.total_stock' => 'desc', 'a.input_date' => 'asc');
        //     $this->db->order_by(key($order), $order[key($order)]);
        // }
    }

    public function data_report_stock(){
        $this->_data_report_stock();
        if ($_POST['length'] != -1) {
            $this->db->limit(10, $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function records_filter_report_stock(){
        $this->_get_data_sub_item($itemID);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function records_total_report_stock(){
        $this->db->select('*');
        $this->db->from('m_stock');
        $query = $this->db->get();
        return $query->num_rows();
    }
}
