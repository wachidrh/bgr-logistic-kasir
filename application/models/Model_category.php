<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_category extends CI_Model
{
    var $search_category = array('category_name', 'category_desc');
    var $order_category = array('category_name', 'category_desc', 'flag');

    public function _get_data_category(){
        $this->db->select('*');
        $this->db->from('m_category');

        $i = 0;
        if (isset($_POST['search']) and !empty($_POST['search'])) {
            foreach ($this->search_category as $item) {
                if (($_POST['search'])) {
                    if ($i === 0) {
                        $this->db->group_start();
                        $this->db->like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    } else {
                        $this->db->or_like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    }

                    if (count($this->search_category) - 1 == $i)
                        $this->db->group_end();
                }
                $i++;
            }
        }
        if (isset($_POST['order'])) {
            $this->db->order_by($this->order_category[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $order = array('category_id' => 'desc');
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_data_category(){
        $this->_get_data_category();
        if ($_POST['length'] != -1) {
            $this->db->limit(10, $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function records_filter_category(){
        $this->_get_data_category();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function records_total_category(){
        $this->db->select('*');
        $this->db->from('m_category');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function detail_category($categoryId){
        $this->db->select('*');
        $data = $this->db->get_where('m_category', array('category_id' => $categoryId));
        return $data->row_array();
    }
}
