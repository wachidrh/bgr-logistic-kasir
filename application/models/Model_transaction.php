<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_transaction extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    var $search_cart_item = array();
    var $order_cart_item = array();

    var $search_paid = array('b.item_name','b.item_sku');
    var $order_paid = array('a.purchase_date');

    var $search_invoice = array();
    var $order_invoice = array();

    public function _get_data_cart_item(){
        $this->db->select('a.flag,a.item_id,a.subitem_id,a.uom_id,b.item_name,c.uom_name,(select sum(quantity) from tbl_purchase where item_id = a.item_id and uom_id = a.uom_id and user_id = "'.$_SESSION['id_login'].'" AND transaction_id is null) as quantity,(select sum(subtotal) from tbl_purchase where item_id = a.item_id and uom_id = a.uom_id AND user_id = "'.$_SESSION['id_login'].'" AND transaction_id is null) as subtotal, ( SELECT last_stock from m_stock where stock_id = a.subitem_id ) AS last_stock,');
        $this->db->from('tbl_purchase a');
        $this->db->join('m_item b', 'a.item_id = b.item_id', 'left');
        $this->db->join('m_uom c', 'a.uom_id = c.uom_id', 'left');
        $this->db->where('user_id', $_SESSION['id_login']);
        $this->db->where('transaction_id', null);
        $this->db->group_by('a.item_id,a.uom_id,a.subitem_id,a.flag');

        $i = 0;
        if (isset($_POST['search']) and !empty($_POST['search'])) {
            foreach ($this->search_cart_item as $item) {
                if (($_POST['search'])) {
                    if ($i === 0) {
                        $this->db->group_start();
                        $this->db->like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    } else {
                        $this->db->or_like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    }

                    if (count($this->search_cart_item) - 1 == $i)
                        $this->db->group_end();
                }
                $i++;
            }
        }
    }

    public function get_data_cart_item(){
        $this->_get_data_cart_item();
        $query = $this->db->get();
        return $query->result_array();
    }

    public function records_filter_cart_item(){
        $this->_get_data_cart_item();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function records_total_cart_item(){
        $this->db->select('*');
        $this->db->from('tbl_purchase');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_item_cart(){
        $this->db->select('a.item_id,a.subitem_id,a.uom_id,b.item_name,c.uom_name,(select sum(quantity) from tbl_purchase where item_id = a.item_id and uom_id = a.uom_id and user_id = "'.$_SESSION['id_login'].'" AND transaction_id is null) as quantity,(select sum(subtotal) from tbl_purchase where item_id = a.item_id and uom_id = a.uom_id AND user_id = "'.$_SESSION['id_login'].'" AND transaction_id is null) as subtotal');
        $this->db->from('tbl_purchase a');
        $this->db->join('m_item b', 'a.item_id = b.item_id', 'left');
        $this->db->join('m_uom c', 'a.uom_id = c.uom_id', 'left');
        $this->db->where('user_id', $_SESSION['id_login']);
        $this->db->where('transaction_id', null);
        $this->db->group_by('a.item_id,a.uom_id,a.subitem_id');
        $query = $this->db->get();
        return $query->result_array();
    }

    // Get Paid Transaction
    public function _get_paid_transaction(){
        $where1 = '';
        $where2 = '';

        if (isset($_POST['invoice_id'])) {
            $this->db->where('a.transaction_id', decrypt_url($_POST['invoice_id']));
            $where1 = 'and user_id = a.user_id';
        } else {
            $this->db->where('a.transaction_id !=', null);
            if($_SESSION['user_type'] != 1){
                $where1 = 'and user_id = "'.$_SESSION['id_login'].'"';
                $where2 = $this->db->where('a.user_id', $_SESSION['id_login']);
            }
            $this->db->group_by('a.item_id,a.uom_id,a.subitem_id,DATE_FORMAT(a.purchase_date, "%Y-%m-%d")');
        }

        $this->db->select('a.item_id,a.subitem_id,a.uom_id,b.item_sku,b.item_name,DATE_FORMAT(a.purchase_date, "%Y-%m-%d") as purchase_date,c.uom_name,(select sum(quantity) from tbl_purchase where item_id = a.item_id and uom_id = a.uom_id '.$where1.' AND flag = 1) as quantity,(select sum(subtotal) from tbl_purchase where item_id = a.item_id and uom_id = a.uom_id '.$where1.' AND flag = 1) as subtotal, (select sum(ppn) from tbl_purchase where item_id = a.item_id and uom_id = a.uom_id '.$where1.' AND flag = 1) as ppn');
        $this->db->from('tbl_purchase a');
        $this->db->join('m_item b', 'a.item_id = b.item_id', 'left');
        $this->db->join('m_uom c', 'a.uom_id = c.uom_id', 'left');
        $where2;
        $this->db->where('a.flag', 1);
        

        

        if(isset($_POST['start_date']) && isset($_POST['end_date'])){
            $start_date = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['start_date'])));
            $end_date = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['end_date'])));

            $this->db->where('DATE_FORMAT(a.purchase_date, "%Y-%m-%d") >= "'.$start_date.'" AND DATE_FORMAT(a.purchase_date, "%Y-%m-%d") <= "'.$end_date.'"');
        }
        
        $i = 0;
        if (isset($_POST['search']) and !empty($_POST['search'])) {
            foreach ($this->search_paid as $item) {
                if (($_POST['search'])) {
                    if ($i === 0) {
                        $this->db->group_start();
                        $this->db->like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    } else {
                        $this->db->or_like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    }

                    if (count($this->search_paid) - 1 == $i)
                        $this->db->group_end();
                }
                $i++;
            }
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->order_paid[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $order = array('purchase_date' => 'desc');
            $this->db->order_by(key($order), $order[key($order)]);
        }

    }

    public function get_paid_transaction(){
        $this->_get_paid_transaction();
        $query = $this->db->get();
        return $query->result_array();
    }

    public function records_filter_paid(){
        $this->_get_paid_transaction();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function records_total_paid(){
        $this->db->select('*');
        $this->db->from('tbl_purchase');
        $query = $this->db->get();
        return $query->num_rows();
    }

    // Get Transaction Invoice
    public function _get_transaction_invoice(){
        $this->db->select('a.*,b.payment_name');
        $this->db->from('tbl_transaction a');
        $this->db->join('m_payment_method b', 'a.payment_type = b.payment_id', 'left');

        if(isset($_POST['start_date']) && isset($_POST['end_date'])){
            $start_date = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['start_date'])));
            $end_date = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['end_date'])));

            $this->db->where('DATE_FORMAT(a.transaction_date, "%Y-%m-%d") >= "'.$start_date.'" AND DATE_FORMAT(a.transaction_date, "%Y-%m-%d") <= "'.$end_date.'"');
        }
        
        $i = 0;
        if (isset($_POST['search']) and !empty($_POST['search'])) {
            foreach ($this->search_invoice as $item) {
                if (($_POST['search'])) {
                    if ($i === 0) {
                        $this->db->group_start();
                        $this->db->like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    } else {
                        $this->db->or_like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    }

                    if (count($this->search_invoice) - 1 == $i)
                        $this->db->group_end();
                }
                $i++;
            }
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->order_invoice[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $order = array('transaction_date' => 'desc');
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_transaction_invoice(){
        $this->_get_transaction_invoice();
        $query = $this->db->get();
        return $query->result_array();
    }

    public function records_filter_invoice(){
        $this->_get_transaction_invoice();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function records_total_invoice(){
        $this->db->select('*');
        $this->db->from('tbl_transaction');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function detail_invoice($invoiceid){
        $this->db->select('a.*,b.payment_name,c.fullname');
        $this->db->from('tbl_transaction a');
        $this->db->join('m_payment_method b', 'a.payment_type = b.payment_id');
        $this->db->join('tbl_login c', 'a.input_by = c.id_login');
        $this->db->where('a.transaction_id', $invoiceid);
        $data = $this->db->get();
        return $data->row_array();
    }
}