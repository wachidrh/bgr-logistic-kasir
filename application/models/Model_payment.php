<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_payment extends CI_Model
{
    var $search_payment = array('payment_name', 'payment_desc');
    var $order_payment = array('payment_name', 'payment_desc', 'flag');

    public function _get_data_payment(){
        $this->db->select('*');
        $this->db->from('m_payment_method');

        $i = 0;
        if (isset($_POST['search']) and !empty($_POST['search'])) {
            foreach ($this->search_payment as $item) {
                if (($_POST['search'])) {
                    if ($i === 0) {
                        $this->db->group_start();
                        $this->db->like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    } else {
                        $this->db->or_like($item, $this->security->xss_clean(strtolower($this->input->post('search'))));
                    }

                    if (count($this->search_payment) - 1 == $i)
                        $this->db->group_end();
                }
                $i++;
            }
        }
        if (isset($_POST['order'])) {
            $this->db->order_by($this->order_payment[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $order = array('payment_id' => 'desc');
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_data_payment(){
        $this->_get_data_payment();
        if ($_POST['length'] != -1) {
            $this->db->limit(10, $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function records_filter_payment(){
        $this->_get_data_payment();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function records_total_payment(){
        $this->db->select('*');
        $this->db->from('m_payment_method');
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function detail_payment($paymentId){
        $this->db->select('*');
        $data = $this->db->get_where('m_payment_method', array('payment_id' => $paymentId));
        return $data->row_array();
    }
}
