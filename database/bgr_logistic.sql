/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : localhost:8889
 Source Schema         : bgr_logistic

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 04/09/2022 16:21:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for m_category
-- ----------------------------
DROP TABLE IF EXISTS `m_category`;
CREATE TABLE `m_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  `category_desc` varchar(255) DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `flag` int(11) DEFAULT '1',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_category
-- ----------------------------
BEGIN;
INSERT INTO `m_category` (`category_id`, `category_name`, `category_desc`, `input_date`, `update_date`, `flag`) VALUES (1, 'Minuman', NULL, '2022-09-02 22:43:52', NULL, 1);
INSERT INTO `m_category` (`category_id`, `category_name`, `category_desc`, `input_date`, `update_date`, `flag`) VALUES (2, 'Makanan', '', '2022-09-02 22:43:59', '2022-09-03 02:54:56', 1);
INSERT INTO `m_category` (`category_id`, `category_name`, `category_desc`, `input_date`, `update_date`, `flag`) VALUES (3, 'Obat', '', '2022-09-04 05:14:10', '2022-09-04 05:14:23', 1);
INSERT INTO `m_category` (`category_id`, `category_name`, `category_desc`, `input_date`, `update_date`, `flag`) VALUES (4, 'Kaos', '', '2022-09-04 12:53:50', '2022-09-04 15:39:06', 1);
COMMIT;

-- ----------------------------
-- Table structure for m_item
-- ----------------------------
DROP TABLE IF EXISTS `m_item`;
CREATE TABLE `m_item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_sku` varchar(255) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `flag` int(11) DEFAULT '1',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_item
-- ----------------------------
BEGIN;
INSERT INTO `m_item` (`item_id`, `item_sku`, `item_name`, `category_id`, `input_date`, `update_date`, `flag`) VALUES (1, 'BGR1662243087', 'Ultramilk Coklat 250 ml', 1, '2022-09-04 05:11:31', '2022-09-04 05:15:20', 1);
INSERT INTO `m_item` (`item_id`, `item_sku`, `item_name`, `category_id`, `input_date`, `update_date`, `flag`) VALUES (2, 'BGR1662267637', 'Komix Herbal 15 ml', 3, '2022-09-04 12:00:52', NULL, 1);
INSERT INTO `m_item` (`item_id`, `item_sku`, `item_name`, `category_id`, `input_date`, `update_date`, `flag`) VALUES (3, 'BGR1662270889', 'Kaos Polos Gambar', 4, '2022-09-04 12:55:12', NULL, 1);
COMMIT;

-- ----------------------------
-- Table structure for m_payment_method
-- ----------------------------
DROP TABLE IF EXISTS `m_payment_method`;
CREATE TABLE `m_payment_method` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_name` varchar(255) DEFAULT NULL,
  `payment_desc` varchar(255) DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `flag` int(11) DEFAULT '1',
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_payment_method
-- ----------------------------
BEGIN;
INSERT INTO `m_payment_method` (`payment_id`, `payment_name`, `payment_desc`, `insert_date`, `update_date`, `flag`) VALUES (1, 'Cash', NULL, '2022-09-02 22:33:06', '2022-09-03 03:07:59', 1);
INSERT INTO `m_payment_method` (`payment_id`, `payment_name`, `payment_desc`, `insert_date`, `update_date`, `flag`) VALUES (2, 'Debit Card', NULL, '2022-09-02 22:33:16', NULL, 1);
INSERT INTO `m_payment_method` (`payment_id`, `payment_name`, `payment_desc`, `insert_date`, `update_date`, `flag`) VALUES (3, 'Credit Card', NULL, '2022-09-02 22:33:35', NULL, 1);
INSERT INTO `m_payment_method` (`payment_id`, `payment_name`, `payment_desc`, `insert_date`, `update_date`, `flag`) VALUES (4, 'Transfer BCA', '', '2022-09-03 03:07:10', '2022-09-03 03:07:48', 1);
INSERT INTO `m_payment_method` (`payment_id`, `payment_name`, `payment_desc`, `insert_date`, `update_date`, `flag`) VALUES (5, 'Transfer Bank Mandiri', '', '2022-09-04 05:14:39', '2022-09-04 15:39:11', 1);
INSERT INTO `m_payment_method` (`payment_id`, `payment_name`, `payment_desc`, `insert_date`, `update_date`, `flag`) VALUES (6, 'Transfer Bank BNI', 'Transfer melalui bank BNI', '2022-09-04 12:54:12', '2022-09-04 12:54:45', 1);
COMMIT;

-- ----------------------------
-- Table structure for m_stock
-- ----------------------------
DROP TABLE IF EXISTS `m_stock`;
CREATE TABLE `m_stock` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `total_stock` int(11) DEFAULT NULL,
  `last_stock` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`stock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_stock
-- ----------------------------
BEGIN;
INSERT INTO `m_stock` (`stock_id`, `item_id`, `uom_id`, `total_stock`, `last_stock`, `price`, `input_date`, `update_date`) VALUES (1, 1, 4, 100, 98, 50000, '2022-09-04 05:15:32', NULL);
INSERT INTO `m_stock` (`stock_id`, `item_id`, `uom_id`, `total_stock`, `last_stock`, `price`, `input_date`, `update_date`) VALUES (2, 1, 2, 1000, 970, 6500, '2022-09-04 05:16:18', NULL);
INSERT INTO `m_stock` (`stock_id`, `item_id`, `uom_id`, `total_stock`, `last_stock`, `price`, `input_date`, `update_date`) VALUES (3, 2, 2, 50, 18, 25000, '2022-09-04 12:01:09', NULL);
INSERT INTO `m_stock` (`stock_id`, `item_id`, `uom_id`, `total_stock`, `last_stock`, `price`, `input_date`, `update_date`) VALUES (4, 2, 4, 100, 94, 150000, '2022-09-04 12:39:03', NULL);
INSERT INTO `m_stock` (`stock_id`, `item_id`, `uom_id`, `total_stock`, `last_stock`, `price`, `input_date`, `update_date`) VALUES (5, 2, 2, 100, 100, 21500, '2022-09-04 12:39:18', NULL);
INSERT INTO `m_stock` (`stock_id`, `item_id`, `uom_id`, `total_stock`, `last_stock`, `price`, `input_date`, `update_date`) VALUES (6, 3, 2, 1000, 860, 89500, '2022-09-04 12:55:42', NULL);
COMMIT;

-- ----------------------------
-- Table structure for m_uom
-- ----------------------------
DROP TABLE IF EXISTS `m_uom`;
CREATE TABLE `m_uom` (
  `uom_id` int(11) NOT NULL AUTO_INCREMENT,
  `uom_name` varchar(255) DEFAULT NULL,
  `uom_desc` varchar(255) DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `flag` int(11) DEFAULT '1',
  PRIMARY KEY (`uom_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m_uom
-- ----------------------------
BEGIN;
INSERT INTO `m_uom` (`uom_id`, `uom_name`, `uom_desc`, `insert_date`, `update_date`, `flag`) VALUES (1, 'Pack', NULL, '2022-09-02 22:39:46', NULL, 1);
INSERT INTO `m_uom` (`uom_id`, `uom_name`, `uom_desc`, `insert_date`, `update_date`, `flag`) VALUES (2, 'Pcs', NULL, '2022-09-02 22:39:59', NULL, 1);
INSERT INTO `m_uom` (`uom_id`, `uom_name`, `uom_desc`, `insert_date`, `update_date`, `flag`) VALUES (3, 'Meter', '', '2022-09-02 23:01:24', '2022-09-03 14:03:57', 1);
INSERT INTO `m_uom` (`uom_id`, `uom_name`, `uom_desc`, `insert_date`, `update_date`, `flag`) VALUES (4, 'Box', '', '2022-09-04 05:12:57', '2022-09-04 05:13:06', 1);
INSERT INTO `m_uom` (`uom_id`, `uom_name`, `uom_desc`, `insert_date`, `update_date`, `flag`) VALUES (5, 'Kg', '', '2022-09-04 12:52:46', '2022-09-04 15:39:02', 1);
COMMIT;

-- ----------------------------
-- Table structure for tbl_access
-- ----------------------------
DROP TABLE IF EXISTS `tbl_access`;
CREATE TABLE `tbl_access` (
  `access_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_login` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `submenu_id` int(11) DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  PRIMARY KEY (`access_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_access
-- ----------------------------
BEGIN;
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (1, 1, 1, 0, '2022-09-04 13:08:14');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (2, 1, 2, 0, '2022-09-04 13:13:36');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (3, 1, 3, 1, '2022-09-04 13:15:40');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (4, 1, 3, 2, '2022-09-04 13:15:53');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (5, 1, 3, 3, '2022-09-04 13:16:00');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (6, 1, 3, 4, '2022-09-04 13:16:06');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (7, 1, 3, 5, '2022-09-04 13:16:59');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (8, 1, 4, 6, '2022-09-04 13:18:36');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (9, 1, 4, 7, '2022-09-04 13:18:43');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (10, 1, 1, 8, '2022-09-04 13:49:59');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (11, 1, 2, 9, '2022-09-04 13:51:09');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (17, 5, 3, 1, '2022-09-04 14:14:30');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (18, 5, 3, 2, '2022-09-04 14:14:30');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (19, 5, 3, 3, '2022-09-04 14:14:30');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (20, 5, 3, 4, '2022-09-04 14:14:30');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (21, 5, 4, 7, '2022-09-04 14:14:30');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (26, 4, 1, 8, '2022-09-04 02:50:43');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (27, 4, 2, 9, '2022-09-04 02:50:43');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (28, 6, 1, 8, '2022-09-04 03:31:33');
INSERT INTO `tbl_access` (`access_id`, `id_login`, `menu_id`, `submenu_id`, `input_date`) VALUES (29, 6, 2, 9, '2022-09-04 03:31:33');
COMMIT;

-- ----------------------------
-- Table structure for tbl_login
-- ----------------------------
DROP TABLE IF EXISTS `tbl_login`;
CREATE TABLE `tbl_login` (
  `id_login` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `input_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `flag` int(11) DEFAULT '1',
  PRIMARY KEY (`id_login`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_login
-- ----------------------------
BEGIN;
INSERT INTO `tbl_login` (`id_login`, `username`, `password`, `email`, `fullname`, `user_type`, `input_date`, `update_date`, `flag`) VALUES (1, 'wachidrh', 'fe703d258c7ef5f50b71e06565a65aa07194907f', 'wachid.dev@gmail.com', 'Wachid Rahmad', 1, '2022-09-01 14:46:59', NULL, 1);
INSERT INTO `tbl_login` (`id_login`, `username`, `password`, `email`, `fullname`, `user_type`, `input_date`, `update_date`, `flag`) VALUES (4, 'kasir1', 'fe703d258c7ef5f50b71e06565a65aa07194907f', 'kasir.1@gmail.com', 'Kasir 1', 2, '2022-09-04 14:10:25', '2022-09-04 02:50:43', 1);
INSERT INTO `tbl_login` (`id_login`, `username`, `password`, `email`, `fullname`, `user_type`, `input_date`, `update_date`, `flag`) VALUES (5, 'stock1', 'fe703d258c7ef5f50b71e06565a65aa07194907f', 'admin.stock1@gmail.com', 'Admin Stock 1', 3, '2022-09-04 14:14:30', NULL, 1);
INSERT INTO `tbl_login` (`id_login`, `username`, `password`, `email`, `fullname`, `user_type`, `input_date`, `update_date`, `flag`) VALUES (6, 'kasir2', 'fe703d258c7ef5f50b71e06565a65aa07194907f', 'kasir2@gmail.com', 'Kasir 2', 2, '2022-09-04 14:20:12', '2022-09-04 03:31:33', 1);
COMMIT;

-- ----------------------------
-- Table structure for tbl_menu
-- ----------------------------
DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE `tbl_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) DEFAULT NULL,
  `orderkey` int(11) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `nav_id` varchar(255) DEFAULT NULL,
  `span_key` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `flag` int(11) DEFAULT '1',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_menu
-- ----------------------------
BEGIN;
INSERT INTO `tbl_menu` (`menu_id`, `menu_name`, `orderkey`, `icon`, `nav_id`, `span_key`, `link`, `flag`) VALUES (1, 'Dashboard', 1, 'bx bx-home-circle', 'topnav-dashboard', 't-dashboar', 'dashboard', 1);
INSERT INTO `tbl_menu` (`menu_id`, `menu_name`, `orderkey`, `icon`, `nav_id`, `span_key`, `link`, `flag`) VALUES (2, 'Transaksi', 2, 'bx bx-file', 'topnav-transaction', 't-transaction', 'transaction', 1);
INSERT INTO `tbl_menu` (`menu_id`, `menu_name`, `orderkey`, `icon`, `nav_id`, `span_key`, `link`, `flag`) VALUES (3, 'Master', 3, 'bx bx-data', 'topnav-master', 't-master', NULL, 1);
INSERT INTO `tbl_menu` (`menu_id`, `menu_name`, `orderkey`, `icon`, `nav_id`, `span_key`, `link`, `flag`) VALUES (4, 'Report', 4, 'bx bxs-report', 'topnav-report', 't-report', NULL, 1);
COMMIT;

-- ----------------------------
-- Table structure for tbl_purchase
-- ----------------------------
DROP TABLE IF EXISTS `tbl_purchase`;
CREATE TABLE `tbl_purchase` (
  `purchase_id` int(11) NOT NULL AUTO_INCREMENT,
  `subitem_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  `ppn` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `purchase_date` datetime DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_purchase
-- ----------------------------
BEGIN;
INSERT INTO `tbl_purchase` (`purchase_id`, `subitem_id`, `item_id`, `uom_id`, `quantity`, `subtotal`, `ppn`, `user_id`, `purchase_date`, `transaction_id`, `flag`) VALUES (1, 3, 2, 2, 15, 375000, 41250, 4, '2022-09-04 16:14:12', 1, 1);
INSERT INTO `tbl_purchase` (`purchase_id`, `subitem_id`, `item_id`, `uom_id`, `quantity`, `subtotal`, `ppn`, `user_id`, `purchase_date`, `transaction_id`, `flag`) VALUES (2, 3, 2, 2, 7, 175000, 19250, 6, '2022-09-04 16:14:31', 2, 1);
INSERT INTO `tbl_purchase` (`purchase_id`, `subitem_id`, `item_id`, `uom_id`, `quantity`, `subtotal`, `ppn`, `user_id`, `purchase_date`, `transaction_id`, `flag`) VALUES (3, 4, 2, 4, 1, 150000, 16500, 4, '2022-09-04 16:17:43', 3, 1);
COMMIT;

-- ----------------------------
-- Table structure for tbl_submenu
-- ----------------------------
DROP TABLE IF EXISTS `tbl_submenu`;
CREATE TABLE `tbl_submenu` (
  `submenu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `submenu_name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `orderkey` int(11) DEFAULT NULL,
  `flag` int(11) DEFAULT '1',
  PRIMARY KEY (`submenu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_submenu
-- ----------------------------
BEGIN;
INSERT INTO `tbl_submenu` (`submenu_id`, `menu_id`, `submenu_name`, `link`, `orderkey`, `flag`) VALUES (1, 3, 'Unit of Measurement (UoM)', 'uom', 1, 1);
INSERT INTO `tbl_submenu` (`submenu_id`, `menu_id`, `submenu_name`, `link`, `orderkey`, `flag`) VALUES (2, 3, 'Kategori', 'category', 2, 1);
INSERT INTO `tbl_submenu` (`submenu_id`, `menu_id`, `submenu_name`, `link`, `orderkey`, `flag`) VALUES (3, 3, 'Metode Pembayaran', 'payment', 3, 1);
INSERT INTO `tbl_submenu` (`submenu_id`, `menu_id`, `submenu_name`, `link`, `orderkey`, `flag`) VALUES (4, 3, 'Item', 'item', 4, 1);
INSERT INTO `tbl_submenu` (`submenu_id`, `menu_id`, `submenu_name`, `link`, `orderkey`, `flag`) VALUES (5, 3, 'User & Hak Akses', 'user-access', 5, 1);
INSERT INTO `tbl_submenu` (`submenu_id`, `menu_id`, `submenu_name`, `link`, `orderkey`, `flag`) VALUES (6, 4, 'Transaksi', 'transaction/report', 1, 1);
INSERT INTO `tbl_submenu` (`submenu_id`, `menu_id`, `submenu_name`, `link`, `orderkey`, `flag`) VALUES (7, 4, 'Stock', 'item/report-stock', 2, 1);
INSERT INTO `tbl_submenu` (`submenu_id`, `menu_id`, `submenu_name`, `link`, `orderkey`, `flag`) VALUES (8, 1, 'Dashboard', 'dashboard', 1, 1);
INSERT INTO `tbl_submenu` (`submenu_id`, `menu_id`, `submenu_name`, `link`, `orderkey`, `flag`) VALUES (9, 2, 'Transaksi', 'transaction', 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for tbl_transaction
-- ----------------------------
DROP TABLE IF EXISTS `tbl_transaction`;
CREATE TABLE `tbl_transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_number` varchar(255) DEFAULT NULL,
  `payment_type` int(11) DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  `input_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`transaction_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_transaction
-- ----------------------------
BEGIN;
INSERT INTO `tbl_transaction` (`transaction_id`, `invoice_number`, `payment_type`, `transaction_date`, `input_by`) VALUES (1, 'INV/IX/2022/001', 3, '2022-09-04 16:14:16', 4);
INSERT INTO `tbl_transaction` (`transaction_id`, `invoice_number`, `payment_type`, `transaction_date`, `input_by`) VALUES (2, 'INV/IX/2022/002', 5, '2022-09-04 16:14:36', 6);
INSERT INTO `tbl_transaction` (`transaction_id`, `invoice_number`, `payment_type`, `transaction_date`, `input_by`) VALUES (3, 'INV/IX/2022/003', 2, '2022-09-04 16:17:47', 4);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
