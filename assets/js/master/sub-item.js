$("#_sub_uom").select2({
  width: "100%",
  dropdownParent: $("#modalAddSubItem"),
});

var price = document.getElementById("_sub_price");
price.addEventListener("keyup", function (e) {
  price.value = formatRupiah(this.value, "Rp. ");
});

$(document).ready(function () {
  url = hostname + "item/data-sub-item/" + $("#item-id").val();
  table = $("#master_sub_item").DataTable({
    processing: true,
    serverSide: true,
    order: [],
    searching: true,
    info: false,
    pagingType: "numbers",
    bLengthChange: false,
    bPaginate: true,
    bProcessing: false,
    language: {
      emptyTable: "Belum ada data",
      zeroRecords: "0 data",
      paginate: {
        previous: "<i class='fa fa-angle-left' aria-hidden='true'></i>",
        next: " <i class='fa fa-angle-right' aria-hidden='true'></i> ",
        first: "<i class='fa fa-angle-double-left' aria-hidden='true'></i> ",
        last: "<i class='fa fa-angle-double-right' aria-hidden='true'></i> ",
      },
    },
    dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',

    ajax: {
      url: url,
      type: "POST",
      data: function (data) {
        data.search = $("#search_sub_item").val();
      },
    },
    fnPreDrawCallback: function () {
      $("tbody").html("");
    },
    drawCallback: function (hasil) {
      var api = this.api();
      var records_displayed = api.page.info().recordsDisplay;
      $(".dataTables_paginate > ul.pagination").addClass(
        "pagination-rounded justify-content-end mb-2"
      );
    },
    columnDefs: [
      {
        targets: [0],
        orderable: true,
        class: "align-middle",
      },
      {
        targets: [1],
        orderable: true,
        class: "align-middle text-center",
      },
      {
        targets: [2],
        orderable: false,
        class: "align-middle text-center",
      },
      {
        targets: [3],
        orderable: false,
        class: "align-middle text-center",
      },
      {
        targets: [4],
        orderable: false,
        class: "align-middle text-center",
      },
      {
        targets: [5],
        orderable: false,
        class: "align-middle text-center",
      },
    ],

    dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',
  });

  $("#search_sub_item").keyup(function (event) {
    table.ajax.reload(null, false);
  });
});

$(document).on("click", ".add-sub-item", function () {
  let id = $(this).attr("data-id");
  let itemName = $(this).attr("data-name");
  let itemSku = $(this).attr("data-sku");
  $("#modalSubItem").modal("show");

  $("#_sub_item_name").val(itemName);
  $("#_sub_sku").val(itemSku);
  $("#_sub_item_id").val(id);
});

$("#form_sub_item").validate({
  rules: {
    _sub_uom: {
      required: true,
    },
    _sub_stock: {
      required: true,
    },
    _sub_price: {
      required: true,
    },
  },
  messages: {
    _add_sub_item_name: "Nama item harus diisi.",
    _sub_uom: "UoM harus dipilih.",
    _sub_stock: "Stok harus diisi.",
    _sub_price: "Harga harus diisi.",
  },
  errorElement: "small",
  errorPlacement: function (error, element) {
    var placement = $(element).data("error");
    if (element.attr("name") == "_sub_uom") {
      error.insertAfter("#err_sub_uom");
    } else {
      error.insertAfter(element);
    }
  },
  submitHandler: function (form, event) {
    var formData = new FormData(form);

    $.ajax({
      type: "POST",
      url: hostname + "item/add-sub-item",
      data: formData,
      contentType: false,
      cache: false,
      processData: false,
      beforeSend: function (data) {
        $(".loading").removeClass("d-none");
      },
      success: function (data) {
        var parse = JSON.parse(data);
        table.ajax.reload(null, false);
        $("#modalAddSubItem").modal("hide");
        $("#form_sub_item").trigger("reset");
      },
      complete: function (data) {
        $(".loading").addClass("d-none");
      },
    });
  },
});
