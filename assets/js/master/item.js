$("#category").select2({
  width: "100%",
  dropdownParent: $("#modalAddItem"),
});

$("#uom").select2({
  width: "100%",
  dropdownParent: $("#modalAddItem"),
});

$("#_category").select2({
  width: "100%",
  dropdownParent: $("#modalUpdateItem"),
});

$("#_uom").select2({
  width: "100%",
  dropdownParent: $("#modalUpdateItem"),
});

$("#_sub_uom").select2({
  width: "100%",
  dropdownParent: $("#modalAddSubItem"),
});

$(document).on("click", "#btn-add-uom", function () {
  var milliseconds = Math.floor(Date.now() / 1000);
  $("#sku").val("BGR" + milliseconds);
});

$(document).ready(function () {
  url = hostname + "item/get-data-item";
  table = $("#master_item").DataTable({
    processing: true,
    serverSide: true,
    order: [],
    searching: true,
    info: false,
    pagingType: "numbers",
    bLengthChange: false,
    bPaginate: true,
    bProcessing: false,
    language: {
      emptyTable: "Data tidak ada",
      zeroRecords: "0 data",
      paginate: {
        previous: "<i class='fa fa-angle-left' aria-hidden='true'></i>",
        next: " <i class='fa fa-angle-right' aria-hidden='true'></i> ",
        first: "<i class='fa fa-angle-double-left' aria-hidden='true'></i> ",
        last: "<i class='fa fa-angle-double-right' aria-hidden='true'></i> ",
      },
    },
    dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',

    ajax: {
      url: url,
      type: "POST",
      data: function (data) {
        data.search = $("#search_item").val();
      },
    },
    fnPreDrawCallback: function () {
      $("tbody").html("");
    },
    drawCallback: function (hasil) {
      var api = this.api();
      var records_displayed = api.page.info().recordsDisplay;
      $(".dataTables_paginate > ul.pagination").addClass(
        "pagination-rounded justify-content-end mb-2"
      );
    },
    columnDefs: [
      {
        targets: [0],
        orderable: true,
        class: "align-middle",
      },
      {
        targets: [1],
        orderable: true,
        class: "align-middle",
      },
      {
        targets: [2],
        orderable: false,
        class: "align-middle text-center",
      },
      {
        targets: [3],
        orderable: false,
        class: "align-middle text-center",
      },
      {
        targets: [4],
        orderable: false,
        class: "align-middle text-center",
      },
    ],

    dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',
  });

  $("#search_item").keyup(function (event) {
    table.ajax.reload(null, false);
  });

  $("#form_item").validate({
    rules: {
      sku: {
        required: true,
      },
      category: {
        required: true,
      },
      item_name: {
        required: true,
      },
      uom: {
        required: true,
      },
      stock: {
        required: true,
      },
      price: {
        required: true,
      },
    },
    messages: {
      sku: "SKU harus diisi.",
      category: "Kategori harus dipilih.",
      item_name: "Nama item harus diisi.",
      uom: "UoM harus dipilih.",
      stock: "Stok harus diisi.",
      price: "Harga harus diisi.",
    },
    errorElement: "small",
    errorPlacement: function (error, element) {
      var placement = $(element).data("error");
      if (element.attr("name") == "category") {
        error.insertAfter("#err_category");
      } else if (element.attr("name") == "uom") {
        error.insertAfter("#err_uom");
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function (form, event) {
      var formData = new FormData(form);

      $.ajax({
        type: "POST",
        url: hostname + "item/add-item",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function (data) {
          $(".loading").removeClass("d-none");
        },
        success: function (data) {
          var parse = JSON.parse(data);
          if (parse.status == true) {
            Swal.fire("Success", "Berhasil menambahkan data item.", "success");
            table.ajax.reload(null, false);
          } else {
            Swal.fire("Failed", "Gagal menambahkan data item.", "error");
          }
          $("#modalAddItem").modal("hide");
          $("#form_item").trigger("reset");
        },
        complete: function (data) {
          $(".loading").addClass("d-none");
        },
      });
    },
  });

  $(document).on("click", ".btn-inactive-item", function (e) {
    let id = $(this).attr("data-id");
    let flag = $(this).attr("data-flag");

    if (flag == 1) {
      titleData = "Yakin menon-aktifkan data ini?";
      textData =
        "Apabila data ini dinon-aktifkan, maka tidak dapat digunakan ketika menambah item.";
    } else {
      titleData = "Yakin meng-aktifkan data ini?";
      textData = "Data ini dapat digunakan kembali ketika menambahkan item.";
    }

    Swal.fire({
      title: titleData,
      text: textData,
      icon: "warning",
      showCancelButton: !0,
      confirmButtonColor: "#34c38f",
      cancelButtonColor: "#f46a6a",
      cancelButtonText: "Batal",
      confirmButtonText: "Ya",
    }).then(function (t) {
      if (t.value) {
        $.ajax({
          type: "POST",
          url: hostname + "item/update-flag-item",
          data: {
            id: id,
            flag: flag,
          },
          success: function (data) {
            var parse = JSON.parse(data);
            if (parse.status == true) {
              Swal.fire("Success", "Data berasil diperbarui", "success");
              table.ajax.reload(null, false);
            } else {
              Swal.fire("Failed", "Data gagal diperbarui", "error");
            }
          },
        });
      }
    });
  });
});

$(document).on("click", ".btn-update-item", function () {
  let id = $(this).attr("data-id");
  $.ajax({
    type: "POST",
    url: hostname + "item/get-detail-item",
    data: { id: id },
    dataType: "json",
    success: function (data) {
      $("#_item_id").val(id);
      $("#_sku").val(data.item_sku);
      $("#_category").val(data.category_id).trigger("change");
      $("#_item_name").val(data.item_name);
      $("#modalUpdateItem").modal("show");
    },
  });

  $("#form_update_item").validate({
    rules: {
      sku: {
        required: true,
      },
      category: {
        required: true,
      },
      item_name: {
        required: true,
      },
    },
    messages: {
      sku: "SKU harus diisi.",
      category: "Kategori harus dipilih.",
      item_name: "Nama item harus diisi.",
    },
    errorElement: "small",
    errorPlacement: function (error, element) {
      var placement = $(element).data("error");
      if (element.attr("name") == "_category") {
        error.insertAfter("#_err_category");
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function (form, event) {
      var formData = new FormData(form);

      $.ajax({
        type: "POST",
        url: hostname + "item/update-item",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function (data) {
          $(".loading").removeClass("d-none");
        },
        success: function (data) {
          var parse = JSON.parse(data);
          if (parse.status == true) {
            Swal.fire("Success", "Item berasil diperbarui", "success");
            table.ajax.reload(null, false);
          } else {
            Swal.fire("Failed", "Item gagal diperbarui", "error");
          }
          $("#modalUpdateItem").modal("hide");
        },
        complete: function (data) {
          $(".loading").addClass("d-none");
        },
      });
    },
  });
});
