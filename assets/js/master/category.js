$(document).ready(function () {
  url = hostname + "category/get-data-category";
  table = $("#master_category").DataTable({
    processing: true,
    serverSide: true,
    order: [],
    searching: true,
    info: false,
    //"pagingType": "listbox",
    pagingType: "numbers",
    bLengthChange: false,
    bPaginate: true,
    bProcessing: false,
    language: {
      emptyTable: "Data tidak ada",
      zeroRecords: "0 data",
      paginate: {
        previous: "<i class='fa fa-angle-left' aria-hidden='true'></i>",
        next: " <i class='fa fa-angle-right' aria-hidden='true'></i> ",
        first: "<i class='fa fa-angle-double-left' aria-hidden='true'></i> ",
        last: "<i class='fa fa-angle-double-right' aria-hidden='true'></i> ",
      },
    },
    dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',

    ajax: {
      url: url,
      type: "POST",
      data: function (data) {
        data.search = $("#search_category").val();
      },
    },
    fnPreDrawCallback: function () {
      $("tbody").html("");
    },
    drawCallback: function (hasil) {
      var api = this.api();
      var records_displayed = api.page.info().recordsDisplay;
      $(".dataTables_paginate > ul.pagination").addClass(
        "pagination-rounded justify-content-end mb-2"
      );
    },
    columnDefs: [
      {
        targets: [0],
        orderable: true,
        class: "align-middle",
      },
      {
        targets: [1],
        orderable: true,
        class: "align-middle",
      },
      {
        targets: [2],
        orderable: true,
        class: "align-middle text-center",
      },
      {
        targets: [3],
        orderable: true,
        class: "align-middle text-center",
      },
    ],

    dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',
  });

  $("#search_category").keyup(function (event) {
    table.ajax.reload(null, false);
  });

  $("#form_category").validate({
    rules: {
      category_name: {
        required: true,
      },
    },
    messages: {
      category_name: "Kategori harus diisi.",
    },
    errorElement: "small",
    errorPlacement: function (error, element) {
      var placement = $(element).data("error");
      error.insertAfter(element);
    },
    submitHandler: function (form, event) {
      var formData = new FormData(form);

      $.ajax({
        type: "POST",
        url: hostname + "category/add-category",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function (data) {
          $(".loading").removeClass("d-none");
        },
        success: function (data) {
          var parse = JSON.parse(data);
          if (parse.status == true) {
            Swal.fire("Success", "Data berasil diperbarui", "success");
            table.ajax.reload(null, false);
          } else {
            Swal.fire("Failed", "Data gagal diperbarui", "error");
          }
          $("#modalAddcategory").modal("hide");
          $("#form_category").trigger("reset");
        },
        complete: function (data) {
          $(".loading").addClass("d-none");
        },
      });
    },
  });

  $(document).on("click", ".btn-inactive-category", function (e) {
    let id = $(this).attr("data-id");
    let flag = $(this).attr("data-flag");

    if (flag == 1) {
      titleData = "Yakin menon-aktifkan data ini?";
      textData =
        "Apabila data ini dinon-aktifkan, maka tidak dapat digunakan ketika menambah item.";
    } else {
      titleData = "Yakin meng-aktifkan data ini?";
      textData = "Data ini dapat digunakan kembali ketika menambahkan item.";
    }

    Swal.fire({
      title: titleData,
      text: textData,
      icon: "warning",
      showCancelButton: !0,
      confirmButtonColor: "#34c38f",
      cancelButtonColor: "#f46a6a",
      cancelButtonText: "Batal",
      confirmButtonText: "Ya",
    }).then(function (t) {
      if (t.value) {
        $.ajax({
          type: "POST",
          url: hostname + "category/update-flag-category",
          data: {
            id: id,
            flag: flag,
          },
          success: function (data) {
            var parse = JSON.parse(data);
            if (parse.status == true) {
              Swal.fire("Success", "Data berasil diperbarui", "success");
              table.ajax.reload(null, false);
            } else {
              Swal.fire("Failed", "Data gagal diperbarui", "error");
            }
          },
        });
      }
    });
  });
});

$(document).on("click", ".btn-update-category", function () {
  let id = $(this).attr("data-id");
  $.ajax({
    type: "POST",
    url: hostname + "category/get-detail-category",
    data: { id: id },
    dataType: "json",
    success: function (data) {
      $("#_category_id").val(id);
      $("#_category_name").val(data.category_name);
      $("#_category_desc").val(data.category_desc);
      $("#modalUpdatecategory").modal("show");
    },
  });

  $("#form_update_category").validate({
    rules: {
      _category_name: {
        required: true,
      },
    },
    messages: {
      _category_name: "Kategori harus diisi.",
    },
    errorElement: "small",
    errorPlacement: function (error, element) {
      var placement = $(element).data("error");
      error.insertAfter(element);
    },
    submitHandler: function (form, event) {
      var formData = new FormData(form);

      $.ajax({
        type: "POST",
        url: hostname + "category/update-category",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function (data) {
          $(".loading").removeClass("d-none");
        },
        success: function (data) {
          var parse = JSON.parse(data);
          if (parse.status == true) {
            Swal.fire("Success", "Data berasil diperbarui", "success");
            table.ajax.reload(null, false);
          } else {
            Swal.fire("Failed", "Data gagal diperbarui", "error");
          }
          $("#modalUpdatecategory").modal("hide");
        },
        complete: function (data) {
          $(".loading").addClass("d-none");
        },
      });
    },
  });
});
