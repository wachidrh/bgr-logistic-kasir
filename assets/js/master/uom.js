$(document).ready(function () {
  url = hostname + "uom/get-data-uom";
  table = $("#master_uom").DataTable({
    processing: true,
    serverSide: true,
    order: [],
    searching: true,
    info: false,
    //"pagingType": "listbox",
    pagingType: "numbers",
    bLengthChange: false,
    bPaginate: true,
    bProcessing: false,
    language: {
      emptyTable: "Data tidak ada",
      zeroRecords: "0 data",
      paginate: {
        previous: "<i class='fa fa-angle-left' aria-hidden='true'></i>",
        next: " <i class='fa fa-angle-right' aria-hidden='true'></i> ",
        first: "<i class='fa fa-angle-double-left' aria-hidden='true'></i> ",
        last: "<i class='fa fa-angle-double-right' aria-hidden='true'></i> ",
      },
    },
    dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',

    ajax: {
      url: url,
      type: "POST",
      data: function (data) {
        data.search = $("#search_uom").val();
      },
    },
    fnPreDrawCallback: function () {
      $("tbody").html("");
    },
    drawCallback: function (hasil) {
      var api = this.api();
      var records_displayed = api.page.info().recordsDisplay;
      $(".dataTables_paginate > ul.pagination").addClass(
        "pagination-rounded justify-content-end mb-2"
      );
    },
    columnDefs: [
      {
        targets: [0],
        orderable: true,
        class: "align-middle",
      },
      {
        targets: [1],
        orderable: true,
        class: "align-middle",
      },
      {
        targets: [2],
        orderable: true,
        class: "align-middle text-center",
      },
      {
        targets: [3],
        orderable: true,
        class: "align-middle text-center",
      },
    ],

    dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',
  });

  $("#search_uom").keyup(function (event) {
    table.ajax.reload(null, false);
  });

  $("#form_uom").validate({
    rules: {
      uom_name: {
        required: true,
      },
    },
    messages: {
      uom_name: "Unit of Measurement harus diisi.",
    },
    errorElement: "small",
    errorPlacement: function (error, element) {
      var placement = $(element).data("error");
      error.insertAfter(element);
    },
    submitHandler: function (form, event) {
      var formData = new FormData(form);

      $.ajax({
        type: "POST",
        url: hostname + "uom/add-uom",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function (data) {
          $(".loading").removeClass("d-none");
        },
        success: function (data) {
          var parse = JSON.parse(data);
          if (parse.status == true) {
            Swal.fire("Success", "Data berasil diperbarui", "success");
            table.ajax.reload(null, false);
          } else {
            Swal.fire("Failed", "Data gagal diperbarui", "error");
          }
          $("#modalAddUom").modal("hide");
          $("#form_uom").trigger("reset");
        },
        complete: function (data) {
          $(".loading").addClass("d-none");
        },
      });
    },
  });

  $(document).on("click", ".btn-inactive-uom", function (e) {
    let id = $(this).attr("data-id");
    let flag = $(this).attr("data-flag");

    if (flag == 1) {
      titleData = "Yakin menon-aktifkan data ini?";
      textData =
        "Apabila data ini dinon-aktifkan, maka tidak dapat digunakan ketika menambah item.";
    } else {
      titleData = "Yakin meng-aktifkan data ini?";
      textData = "Data ini dapat digunakan kembali ketika menambahkan item.";
    }

    Swal.fire({
      title: titleData,
      text: textData,
      icon: "warning",
      showCancelButton: !0,
      confirmButtonColor: "#34c38f",
      cancelButtonColor: "#f46a6a",
      cancelButtonText: "Batal",
      confirmButtonText: "Ya",
    }).then(function (t) {
      if (t.value) {
        $.ajax({
          type: "POST",
          url: hostname + "uom/update-flag-uom",
          data: {
            id: id,
            flag: flag,
          },
          success: function (data) {
            var parse = JSON.parse(data);
            if (parse.status == true) {
              Swal.fire("Success", "Data berasil diperbarui", "success");
              table.ajax.reload(null, false);
            } else {
              Swal.fire("Failed", "Data gagal diperbarui", "error");
            }
          },
        });
      }
    });
  });
});

$(document).on("click", ".btn-update-uom", function () {
  let id = $(this).attr("data-id");
  $.ajax({
    type: "POST",
    url: hostname + "uom/get-detail-uom",
    data: { id: id },
    dataType: "json",
    success: function (data) {
      $("#_uom_id").val(id);
      $("#_uom_name").val(data.uom_name);
      $("#_uom_desc").val(data.uom_desc);
      $("#modalUpdateUom").modal("show");
    },
  });

  $("#form_update_uom").validate({
    rules: {
      _uom_name: {
        required: true,
      },
    },
    messages: {
      _uom_name: "Unit of Measurement harus diisi.",
    },
    errorElement: "small",
    errorPlacement: function (error, element) {
      var placement = $(element).data("error");
      error.insertAfter(element);
    },
    submitHandler: function (form, event) {
      var formData = new FormData(form);

      $.ajax({
        type: "POST",
        url: hostname + "uom/update-uom",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function (data) {
          $(".loading").removeClass("d-none");
        },
        success: function (data) {
          var parse = JSON.parse(data);
          if (parse.status == true) {
            Swal.fire("Success", "Data berasil diperbarui", "success");
            table.ajax.reload(null, false);
          } else {
            Swal.fire("Failed", "Data gagal diperbarui", "error");
          }
          $("#modalUpdateUom").modal("hide");
        },
        complete: function (data) {
          $(".loading").addClass("d-none");
        },
      });
    },
  });
});
