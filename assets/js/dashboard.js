$(document).ready(function () {
  url = hostname + "dashboard/get-data-transaction";
  table = $("#table_dashboard").DataTable({
    processing: true,
    serverSide: true,
    order: [],
    searching: true,
    info: false,
    pagingType: "numbers",
    bLengthChange: false,
    bPaginate: true,
    bProcessing: false,
    language: {
      emptyTable: "Belum ada data",
      zeroRecords: "Data tidak ditemukan",
      paginate: {
        previous: "<i class='fa fa-angle-left' aria-hidden='true'></i>",
        next: " <i class='fa fa-angle-right' aria-hidden='true'></i> ",
        first: "<i class='fa fa-angle-double-left' aria-hidden='true'></i> ",
        last: "<i class='fa fa-angle-double-right' aria-hidden='true'></i> ",
      },
    },
    dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',

    ajax: {
      url: url,
      type: "POST",
      data: function (data) {
        var startDate = $("#startDate").val();
        var endDate = $("#endDate").val();

        data.search = $("#search_item").val();

        if (startDate != "" && endDate != "") {
          data.start_date = startDate;
          data.end_date = endDate;
        }
      },
    },
    fnPreDrawCallback: function () {
      $("tbody").html("");
    },
    drawCallback: function (hasil) {
      var api = this.api();
      var records_displayed = api.page.info().recordsDisplay;
      $(".dataTables_paginate > ul.pagination").addClass(
        "pagination-rounded justify-content-end mb-2"
      );
    },
    columnDefs: [
      {
        targets: [0],
        orderable: true,
        class: "align-middle",
      },
      {
        targets: [1],
        orderable: false,
        class: "align-middle",
      },
      {
        targets: [2],
        orderable: false,
        class: "align-middle",
      },
      {
        targets: [3],
        orderable: false,
        class: "align-middle text-center",
      },
      {
        targets: [4],
        orderable: false,
        class: "align-middle text-center",
      },
      {
        targets: [5],
        orderable: false,
        class: "align-middle",
      },
      {
        targets: [6],
        orderable: false,
        class: "align-middle",
      },
    ],

    dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',
  });

  $("#search_item").keyup(function (event) {
    table.ajax.reload(null, false);
  });
});

$(document).on("click", "#btn-filter-date", function (e) {
  $("#modalFilter").modal("hide");
  table.ajax.reload(null, false);

  $("#startDate").val("");
  $("#endDate").val("");
});
