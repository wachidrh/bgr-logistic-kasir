$(document).ready(function () {
  url = hostname + "transaction/get-invoice-detail/" + $("#invoice-id").val();
  table = $("#master_sub_item").DataTable({
    processing: true,
    serverSide: true,
    order: [],
    searching: true,
    info: false,
    pagingType: "numbers",
    bLengthChange: false,
    bPaginate: true,
    bProcessing: false,
    language: {
      emptyTable: "Belum ada data",
      zeroRecords: "0 data",
      paginate: {
        previous: "<i class='fa fa-angle-left' aria-hidden='true'></i>",
        next: " <i class='fa fa-angle-right' aria-hidden='true'></i> ",
        first: "<i class='fa fa-angle-double-left' aria-hidden='true'></i> ",
        last: "<i class='fa fa-angle-double-right' aria-hidden='true'></i> ",
      },
    },
    dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',

    ajax: {
      url: url,
      type: "POST",
      data: function (data) {
        data.search = $("#search_sub_item").val();
        data.invoice_id = $("#invoice-id").val();
      },
    },
    fnPreDrawCallback: function () {
      $("tbody").html("");
    },
    drawCallback: function (hasil) {
      var api = this.api();
      var records_displayed = api.page.info().recordsDisplay;
      $(".dataTables_paginate > ul.pagination").addClass(
        "pagination-rounded justify-content-end mb-2"
      );
    },
    columnDefs: [
      {
        targets: [0],
        orderable: true,
        class: "align-middle",
      },
      {
        targets: [1],
        orderable: true,
        class: "align-middle",
      },
      {
        targets: [2],
        orderable: false,
        class: "align-middle text-center",
      },
      {
        targets: [3],
        orderable: false,
        class: "align-middle text-center",
      },
      {
        targets: [4],
        orderable: false,
        class: "align-middle",
      },
    ],

    dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',
  });

  $("#search_sub_item").keyup(function (event) {
    table.ajax.reload(null, false);
  });
});
