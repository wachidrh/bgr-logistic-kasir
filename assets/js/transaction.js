$("#item").select2({
  width: "100%",
});

$("#_payment_method").select2({
  width: "100%",
  dropdownParent: $("#modalPay"),
});

$(document).ready(function () {
  url = hostname + "transaction/get-cart-item";
  table = $("#table_cart").DataTable({
    processing: true,
    serverSide: true,
    order: [],
    searching: true,
    info: false,
    pagingType: "numbers",
    bLengthChange: false,
    bPaginate: false,
    bProcessing: false,
    language: {
      emptyTable: "Belum ada data",
      zeroRecords: "Belum ada data",
      paginate: {
        previous: "<i class='fa fa-angle-left' aria-hidden='true'></i>",
        next: " <i class='fa fa-angle-right' aria-hidden='true'></i> ",
        first: "<i class='fa fa-angle-double-left' aria-hidden='true'></i> ",
        last: "<i class='fa fa-angle-double-right' aria-hidden='true'></i> ",
      },
    },
    dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',

    ajax: {
      url: url,
      type: "POST",
    },
    fnPreDrawCallback: function () {
      $("tbody").html("");
    },
    drawCallback: function (hasil) {
      var api = this.api();
      var records_displayed = api.page.info().recordsDisplay;
      $(".dataTables_paginate > ul.pagination").addClass(
        "pagination-rounded justify-content-end mb-2"
      );
    },
    columnDefs: [
      {
        targets: [0],
        orderable: true,
        class: "align-middle",
      },
      {
        targets: [1],
        orderable: true,
        class: "align-middle",
      },
      {
        targets: [2],
        orderable: false,
        class: "align-middle text-center",
      },
      {
        targets: [3],
        orderable: false,
        class: "align-middle text-center",
      },
      {
        targets: [4],
        orderable: false,
        class: "align-middle",
      },
      {
        targets: [5],
        orderable: false,
        class: "align-middle",
      },
      {
        targets: [6],
        orderable: false,
        class: "align-middle",
      },
      {
        targets: [7],
        orderable: false,
        class: "align-middle text-center",
      },
    ],

    dom: '<"top"l>rt<"bottom left"pi><"caption right"><"clear">',
  });

  $("#form_transaction").validate({
    rules: {
      item: {
        required: true,
      },
      uom: {
        required: true,
      },
      qty: {
        required: true,
      },
    },
    messages: {
      item: "Item harus dipilih.",
      uom: "Unit of Measurement harus dipilih.",
      qty: "Quantity harus diisi.",
    },
    errorElement: "small",
    errorPlacement: function (error, element) {
      var placement = $(element).data("error");
      if (element.attr("name") == "item") {
        error.insertAfter("#err_item");
      } else if (element.attr("name") == "uom") {
        error.insertAfter("#err_uom");
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function (form, event) {
      var formData = new FormData(form);

      $.ajax({
        type: "POST",
        url: hostname + "transaction/add-to-cart",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function (data) {
          $(".loading").removeClass("d-none");
        },
        success: function (data) {
          table.ajax.reload(null, false);
          $("#form_transaction").trigger("reset");
          $("#item").val("").trigger("change");
          $("#uom").val("").trigger("change");
          $("#uom").prop("disabled", true);
        },
        complete: function (data) {
          $(".loading").addClass("d-none");
        },
      });
    },
  });

  $("#form_pay_now").validate({
    rules: {
      _payment_method: {
        required: true,
      },
    },
    messages: {
      _payment_method: "Metode pembayaran harus dipilih.",
    },
    errorElement: "small",
    errorPlacement: function (error, element) {
      var placement = $(element).data("error");
      if (element.attr("name") == "_payment_method") {
        error.insertAfter("#_err_payment");
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function (form, event) {
      var formData = new FormData(form);

      $.ajax({
        type: "POST",
        url: hostname + "transaction/purchase",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function (data) {
          $(".loading").removeClass("d-none");
        },
        success: function (data) {
          var payment = JSON.parse(data);

          table.ajax.reload(null, false);
          $("#_payment_method").val("").trigger("change");
          $("#modalPay").modal("hide");

          if (payment.message == "payment_success") {
            Swal.fire("Success!!!", "Pembayaran berhasil.", "success");
          } else if (payment.message == "separated_success") {
            Swal.fire(
              "Perhatian!!!",
              "Pembayaran berhasil, tapi ada item yang gagal bayar.",
              "warning"
            );
          } else {
            Swal.fire("Gagal!!!", "Pembayaran gagal.", "danger");
          }
        },
        complete: function (data) {
          $(".loading").addClass("d-none");
        },
      });
    },
  });
});

$(document).on("change", "#item", function () {
  let id = $(this).val();
  $.ajax({
    type: "POST",
    url: hostname + "transaction/get-avail-item/" + id,
    contentType: false,
    cache: false,
    processData: false,
    beforeSend: function (data) {
      $(".loading").removeClass("d-none");
    },
    success: function (data) {
      var option = JSON.parse(data);
      if (option.status == true) {
        $("#uom").prop("disabled", false);
      } else {
        $("#uom").prop("disabled", true);
      }
      $("#uom").html(option.message);
    },
    complete: function (data) {
      $(".loading").addClass("d-none");
    },
  });
});

$(document).on("click", ".delete-item-cart", function () {
  let id = $(this).attr("data-id");

  $.ajax({
    type: "POST",
    url: hostname + "transaction/delete-item-cart/" + id,
    contentType: false,
    cache: false,
    processData: false,
    beforeSend: function (data) {
      $(".loading").removeClass("d-none");
    },
    success: function (data) {
      table.ajax.reload(null, false);
    },
    complete: function (data) {
      $(".loading").addClass("d-none");
    },
  });
});

$(document).on("click", "#clear-cart", function () {
  $.ajax({
    type: "POST",
    url: hostname + "transaction/delete-item-cart",
    contentType: false,
    cache: false,
    processData: false,
    beforeSend: function (data) {
      $(".loading").removeClass("d-none");
    },
    success: function (data) {
      table.ajax.reload(null, false);
    },
    complete: function (data) {
      $(".loading").addClass("d-none");
    },
  });
});

$(document).on("click", "#summary-transaction", function () {
  $.ajax({
    type: "POST",
    url: hostname + "transaction/summary-item-cart",
    contentType: false,
    cache: false,
    processData: false,
    beforeSend: function (data) {
      $(".loading").removeClass("d-none");
    },
    success: function (data) {
      let summary = JSON.parse(data);

      if (summary.status == false) {
        Swal.fire("", "Item cart kosong.", "warning");
      } else {
        $("#modalPay").modal("show");
        $("#_subtotal").text(summary.message.subtotal);
        $("#_ppn").text(summary.message.ppn);
        $("#_grand_total").text(summary.message.grand_total);
        $("#_payment_method").val("").trigger("change");
      }
    },
    complete: function (data) {
      $(".loading").addClass("d-none");
    },
  });
});
