# BGR Logistic Indonesia - Kasir

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/wachidrh/bgr-logistic-kasir.git
git branch -M main
git push -uf origin main
```

## Credentials

```
#Super Administrator
Username : wachidrh
Password : 12345

#Kasir 1
Username : kasir1
Password : 12345

#Admin Gudang 1
Username : stock1
Password : 12345

Or you can add your own via the Menu Master > User & Hak Akses
```

## Website Demo

```
https://bgr-demo.wachid.dev
```

## Video Demo

```
https://youtu.be/CAGjsJfPPt4
```
